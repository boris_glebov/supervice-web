# -*- coding: utf-8 -*-


__author__ = 'warlock'

import forms
import json
import datetime
import StringIO
import logging
import random
import core.utils as utils

import core.geo as geo

from django.http import  HttpResponse
from django.contrib.auth.decorators import login_required, permission_required
from core.api import CoreApi
from bson.objectid import ObjectId, InvalidId
from core.exceptions import *
from asq.initiators import query

logger = logging.getLogger(__name__)

###
# Employee
###

@login_required
def get_employee_list(request):
    """
    Список сотрудников компании.
    В будущем возможно будет возвращать список сотрудников закрепленных за менеджером
    """
    assert request.is_ajax()

    db = CoreApi().database_api

    response = {
        'status' : True,
        'employees' :
            [
                {
                    'name' : employee.full_name,
                    'id' : str(employee.id),
                    'settings' : {
                        'preset' : employee.settings.preset
                    }
                } for employee in db.employee_list(request.user.company.id)
            ]
    }

    return HttpResponse(json.dumps(response))

@login_required
def save_employee(request):
    assert request.is_ajax()

    if not request.POST:
        response = {
            'status' : False,
            'message' : _get_message_html(True, 'Ошибка в полученных данных')
        }
        return HttpResponse(json.dumps(response))

    # Поулчаем id
    employee_id = request.POST.get('employee_id', '')

    if not employee_id:
        return _create_employee(request.user, request.POST)
    else:
        return _update_employee(request.user, employee_id, request.POST)

def _create_employee(user, post):
    """
    Добавить нового сотрудника
    """
    db_api = CoreApi().database_api

    employee, ex = db_api.employee_create(
        post.get('employee_first_name', ''),
        post.get('employee_last_name', ''),
        post.get('employee_third_name',''),
        post.get('employee_phone', ''),
        user.company
    )

    if ex is not None:
        response = {
            'status' : False,
            'message' : _get_message_html(True, "Проверьте правильность заполненых полей")
        }
    else:
        response = {
            'status' : True,
            'message' : _get_message_html(False, "Пользователь успешно добавлен")
        }

    return HttpResponse(json.dumps(response))

def _update_employee(user, employee_id, post):
    """
    Обновить информацию о сотруднике
    """

    db_api = CoreApi().database_api

    employee = db_api.employee_get(employee_id)
    if not employee:
        response = {
            "status" : False,
            "message": _get_message_html(True, "Пользователь с заданным id не найден.")
        }
        return HttpResponse(json.dumps(response))

    employee.fio.first_name = post.get('employee_first_name', '')
    employee.fio.last_name = post.get('employee_last_name', '')
    employee.fio.third_name = post.get('employee_third_name', '')
    employee.contact.phone = post.get('employee_phone', '')

    try:
        employee.validate()
        employee.save()

        response = {
            'status' : True,
            'message' : _get_message_html(False, "Изменения успешно сохранены")
        }

    except Exception as ex:
        response = {
            'status' : False,
            'message' : _get_message_html(True, "Операция прервана. Проверьте правильность данных")
        }

    return HttpResponse(json.dumps(response))

def _get_message_html(is_error, message):
    alert_type = "alert-success" if not is_error else "alert-error"
    return """
                            <div class="alert alert-block {0}">
                                    <a class="close" data-dismiss="alert" href="#">×</a>
                                    <h4 class="alert-heading">{1}</h4>
                            </div>
                            """.format(alert_type, message)


@login_required
def remove_employee(request, employee_id):
    """
    Пометить сотрудника как удаленный
    """
    assert request.is_ajax()
    db_api = CoreApi().database_api
    db_api.employee_remove(employee_id)

    return HttpResponse('{"status" : True}')

@login_required
def refresh_employee(request):
    """
    Возвращает список сотрудников
    """
    assert request.is_ajax()

    db_api = CoreApi().database_api

    file = StringIO.StringIO()
    counter = 0
    for employee in db_api.employee_list(request.user.company.id):
        try:
            counter += 1
            file.write(u"<tr>")
            file.write(u'<td><input type="hidden" value="{0}"> {1}</td>'.format(employee.id, counter))
            file.write(u'<td name="td_employee_first_name">{0}</td>'.format(employee.fio.first_name))
            file.write(u'<td name="td_employee_last_name">{0}</td>'.format(employee.fio.last_name))
            file.write(u'<td name="td_employee_third_name">{0}</td>'.format(employee.fio.third_name))
            file.write(u'<td name="td_employee_phone">{0}</td>'.format(employee.contact.phone))
            file.write(u'<td></td>')
            file.write(u"</tr>\n")
        except Exception as ex:
            print ex

    content = file.getvalue()
    file.close()
    return HttpResponse(content)

###
# Devices
###
@login_required
def save_device(request):
    assert request.is_ajax()

    if not request.POST:
        response = {
            'status' : False,
            'message' : _get_message_html(True, 'Ошибка в полученных данных')
        }
        return HttpResponse(json.dumps(response))

    # Поулчаем id
    device_id = request.POST.get('device_id', '')

    if not device_id:
        return _create_device(request.user, request.POST)
    else:
        return _update_device(request.user, device_id, request.POST)

def _create_device(user, post):
    db = CoreApi().database_api

    owned_id = post.get('device_owned_id', '')

    device, ex = db.device_create(post.get('device_imei', ''),
        post.get('device_model', ''),
        post.get('device_active', '').lower() == u'on',
        user.company.id,
        owned_id
    )

    if ex is not None:
        response = {
            'status' : False,
            'message' : _get_message_html(True, ex.message)
        }
    else:
        response = {
            'status' : True,
            'message' : _get_message_html(False, 'GPS трекер успешно добавлен')
        }

    return HttpResponse(json.dumps(response))

def _update_device(user, device_id, post):
    db = CoreApi().database_api

    device = db.device_get(device_id)

    if device is None:
        response = {
            'status' : False,
            'message' : _get_message_html(False, 'Устройство не найдено')
        }

        return HttpResponse(json.dumps(response))

    device.imei = post.get('device_imei', '')
    device.model = post.get('device_model', '')
    device.is_active = post.get('device_active', '').lower() == u'on'

    device_id = post.get('device_owned_id', '')
    if device_id:
        device.owned = ObjectId(device_id)

    try:
        if db.device_exists(device.imei, device.id):
            response = {
                'status' : False,
                'message' : _get_message_html(True,'Устройства с таким imei уже зарегистрировано')
            }

            return HttpResponse(json.dumps(response))

        device.validate()
        device.save()

        response = {
            'status' : True,
            'message' : _get_message_html(False,'Данные успешно обновлены')
        }
    except Exception as ex:
        response = {
            'status' : False,
            'message' : _get_message_html(True, 'Ошибка. Проверьте правильность введенных данных')
        }

    return HttpResponse(json.dumps(response))

@login_required
def refresh_devices(request):
    assert request.is_ajax()

    db_api = CoreApi().database_api

    file = StringIO.StringIO()
    counter = 0
    for device in db_api.device_list(request.user.company.id):
        try:
            counter += 1
            file.write(u"<tr>")
            file.write(u"""<td><input type="hidden" value="{0}"> {1}
                <input type="hidden" value="{2}" name="td_device_owned_id">
                <input type="hidden" value="{3}" name="td_device_owned_name">
            </td>

            """.format(device.id, counter, device.owned_id, device.owned_name))
            file.write(u'<td name="td_device_imei">{0}</td>'.format(device.imei))
            file.write(u'<td name="td_device_model">{0}</td>'.format(device.model))
            file.write(u'<td name="td_device_is_active">{0}</td>'.format(u'Включен' if device.is_active else u'Выключен'))
            file.write(u"</tr>\n")
        except Exception as ex:
            print ex

    content = file.getvalue()
    file.close()
    return HttpResponse(content)

@login_required
def remove_device(request, device_id):
    assert request.is_ajax()

    db = CoreApi().database_api

    try:
        db.device_remove(device_id)

        response = {
            'status' : True,
            'message' : 'Устройство удалено.'
        }
    except Exception as ex:
        response = {
            'status' : False,
            'message' : ex.message
        }

    return HttpResponse(json.dumps(response))


###
# REST API
###

###
# Monitoring
###
@login_required
def get_track(request):
    """
    Запрос трэка за один день.
    День за который нужен трэк передается в аргументе POST['date']. Формат 10.02.2011
    """

    assert request.is_ajax()

    db_api = CoreApi().database_api
    service_api = CoreApi().service_api

    try:
        d = datetime.datetime.strptime(request.POST['date'], "%d.%m.%Y")

        employee = db_api.employee_get(request.POST['employee_id'])
        if employee is None:
            return HttpResponse(json.dumps({
                'status' : False,
                'message' : 'Пользователь не найден. Обработка запроса не возможна'
            }))

        track = list(service_api.get_track_per_day(employee, d))
        response = {
            'status' : True,
            'employee' : {
              'id' : str(employee.id),
              'full_name' : employee.full_name
            },
#            'track' : [[point.lat, point.lon] for point in track],
            'track_by_time' : geo.optimization_track([{
                'lat' : point.lat,
                'lon' : point.lon,
                'local_datetime' : point.local_datetime.strftime('%d.%m.%Y %H:%M'),
                'name' : '',
                'speed' : point.speed
            } for point in track])
        }

    except Exception:
        response = {
            'status' : False,
            'message' : 'На сервере произошла ошибка.'
        }

    return HttpResponse(json.dumps(response))

@login_required
def get_last_position(request):
    """
    Запрос на получение последнего местонахождения обьекта. Возвращается одна точка.
    """

    assert request.is_ajax()

    db_api = CoreApi().database_api
    rest_api = CoreApi().service_api

    employees = db_api.employee_list(request.user.company.id)


    tracks = []

    for employee in employees:
        point = rest_api.get_last_position(employee)

        if point is None:
            continue

        last_update_days_count = abs((datetime.datetime.now() - point.local_datetime).seconds / 3600)

        battery = random.randrange(0,100)
        tracks.append({
            'employee' : {
                'id' : str(employee.id),
                'full_name' : employee.full_name,
                'settings' : {
                    'preset' : employee.settings.preset
                }
            },

            'lat' : point.lat,
            'lon' : point.lon,
            'speed' : round(point.speed),
            'curs' : point.curs,
            'date' : point.local_datetime.strftime('%d.%m.%Y %H:%M'),
            'is_undefined' : last_update_days_count > 0,
            'last_update_days_count' : last_update_days_count,
            'battery' : battery
        })

    return HttpResponse(json.dumps(tracks))

@login_required
def get_last_position_by_employee(request):
    """
    Возвращает последнюю точку где находился сотрудник
    """
    assert request.is_ajax()

    if not request.POST or request.POST.get('employee_id', None) is None:
        return HttpResponse(json.dumps({
            'status' : False,
            'message' : u'Неверные параметры.'
        }))

    employee_id = request.POST.get('employee_id')

    db_api = CoreApi().database_api
    service_api = CoreApi().service_api

    employee = db_api.employee_get(employee_id)

    if employee is None:
        return HttpResponse(json.dumps({
            'status' : False,
            'message' : u'Сотрудник не найден.'
        }))


    point = service_api.get_last_position(employee)

    if point is None:
        return HttpResponse(json.dumps({
            'status' : False,
            'message' : u'Точек нет.'
        }))

    return HttpResponse(json.dumps({
       'status' : True,
       'employee' : {
           'id' : employee_id,
           'full_name' : employee.full_name
       },

       'lat' : point.lat,
       'lon' : point.lon,
       'speed' : round(point.speed),
       'curs' : point.curs
    }))

###
# Hisotry
###
def get_days_where_exists_track(request):
    """
    """
    assert request.is_ajax()

###
# Logistics
###

@login_required
def get_sale_points(request):
    """
    Возвращает список торговых точек компании
    """
    assert request.is_ajax()

    db = CoreApi().database_api

    points = [
            {
            'name' : point.name,
            'address' : point.address,
            'legal_name' : point.legal_name,
            'coords' : [point.coords.lat, point.coords.lon],
            'radius' : point.coords.radius,
            'id' : str(point.id),
            'region' : point.region
    } for point in db.sale_point_list(request.user.company.id)]

    return HttpResponse(json.dumps(points))

@login_required
def save_sale_point(request):
    """
    Добавлят новую торговую точку или сохраняет изменения существующей.
    """
    assert request.is_ajax()

    db = CoreApi().database_api

    #Извлекаем параметры
    company_name = request.POST.get('company_name', '')
    legal_name = request.POST.get('owner_name', '')
    lat = float(request.POST.get('lat', 0.0))
    lon = float(request.POST.get('lon', 0.0))
    address = request.POST.get('address', '')
    region = request.POST.get('region', '')
    sale_point_id = request.POST.get('sale_point_id', None)

    try:
        radius = round(float(request.POST.get('radius', '15').replace(',','.'))) #по умолчанию выставляем радиус 15 метров
    except Exception as ex:
        radius = 15

    try:

        if sale_point_id is None:
            sale_point, ex = db.sale_point_create(request.user, company_name,[lat, lon], address, region, legal_name, radius)

            if ex is not None:
                logger.error(ex.message)
                return HttpResponse(json.dumps({
                    'status' : False,
                    'message' : ex.message
                }))

            is_update = False
        else:
            sale_point = db.sale_point_get(ObjectId(sale_point_id), request.user.company.id)

            if sale_point is None:
                return HttpResponse(json.dumps({
                    'status' : False,
                    'message' : 'Торговая точка не существует.'
                }))

            sale_point.name = company_name
            sale_point.legal_name = legal_name
            sale_point.coords.radius = radius

            try:
                sale_point.validate()
                sale_point.save()

                is_update = True
            except Exception as ex:
                logger.error(ex.message)
                return HttpResponse(json.dumps({
                    'status': False,
                    'message' : 'Внутренняя ошибка.'
                }))

        return HttpResponse(json.dumps(
        {
            'status' : True,
            'message' : '',
            'name' : sale_point.name,
            'sale_point_id' : str(sale_point.id),
            'coords' : [sale_point.coords.lat, sale_point.coords.lon],
            'radius' :  radius,
            'address' : sale_point.address,
            'legal_name' : sale_point.legal_name,
            'is_update' : is_update
        }))
    except Exception as ex:
        return HttpResponse(json.dumps({'status' : False, 'message' : ex.message}))

###
# Routing
###

@login_required
def get_current_route(request):
    """
    Получаем текущий маршрут
    """
    assert request.is_ajax()

    db = CoreApi().database_api

    try:
        employee_id = ObjectId(request.POST.get('employee_id', 'None'))
        waypoints = db.waypoint_list(employee_id)

        response = {
            'status' : True,
            'message' : '',
            'route' : waypoints
        }
    except InvalidId:
        response = {
            'status' : False,
            'message' : 'Неверный формат id сотрудника.'
        }
    except Exception as ex:
        response = {
            'status' : False,
            'message' : 'Внутренняя ошибка.'
        }

        print ex

    return HttpResponse(json.dumps(response))

@login_required
def add_sale_point_to_route(request):
    """
    Добавить торговую точку к маршруту клиента
    """
    assert request.is_ajax()

    db = CoreApi().database_api

    try:
        employee_id = ObjectId(request.POST.get('employee_id', 'None'))
        sale_point_id = ObjectId(request.POST.get('sale_point_id', 'None'))

        str_start_date = request.POST.get('start_date', None)
        period = int(request.POST.get('period', -1))
        status = request.POST.get('status', 'true').lower() == 'true'

        if str_start_date is None or period == -1:
            raise EmptyRequiredFillException('Заполнены не все поля.')

        # Пробуем разобрать пришедшую дату:
        start_date = utils.str_to_date(str_start_date)

        waypoint, ex = db.waypoint_add(employee_id, sale_point_id, period, status, start_date)

        if ex is not None:
            return HttpResponse(json.dumps({
                'status' : False,
                'message' : ex.message
            }))

        response = {
            'status' : True,
            'message' : '',
            'waypoint' : {
                'id' : str(waypoint.id),
                'employee_id' : str(waypoint.employee_id),
                'sale_point_id' : str(waypoint.sale_point_id),
                'sale_point_name' : waypoint.sale_point.name,
                'sale_point_address' : waypoint.sale_point.address,
                'sale_point_coords' :  [waypoint.sale_point.coords.lat, waypoint.sale_point.coords.lon],
                'sale_point_legal' : waypoint.sale_point.legal_name,
                'sale_point_radius' : waypoint.sale_point.coords.radius,
                'status' : waypoint.status,
                'period' : waypoint.period,
                'local_startdate' : utils.date_to_str(waypoint.local_startdate)
            }
        }

    except InvalidId:
        response = {
            'status' : False,
            'message' : 'Неверный формат id сотрудника.'
        }
    except EmptyRequiredFillException as emptyRequiredFillException:
        response = {
            'status' : False,
            'message' : emptyRequiredFillException.message
        }
    except Exception as ex:
        response = {
            'status' : False,
            'message'  :'Внутренняя ошибка сервера.'
        }

    return HttpResponse(json.dumps(response))

def update_waypoint(request):
    """
    Обновляем информацию о точке
    """
    assert request.is_ajax()

    db = CoreApi().database_api

    try:
        waypoint_id = ObjectId(request.POST.get('waypoint_id', None))

        str_start_date = request.POST.get('start_date', None)
        period = int(request.POST.get('period', -1))
        status = request.POST.get('status', 'true').lower() == 'true'

        if str_start_date is None or period == -1:
            raise EmptyRequiredFillException('Заполнены не все поля.')

        # Пробуем разобрать пришедшую дату:
        start_date = utils.str_to_date(str_start_date)

        waypoint, ex = db.waypoint_update(waypoint_id, period, status, start_date)

        if ex is not None:
            raise ex

        response = {
            'status' : True,
            'message' : '',
            'waypoint' :
            {
                'id' : str(waypoint.id),
                'employee_id' : str(waypoint.employee_id),
                'sale_point_id' : str(waypoint.sale_point_id),
                'sale_point_name' : waypoint.sale_point.name,
                'sale_point_address' : waypoint.sale_point.address,
                'sale_point_coords' :  [waypoint.sale_point.coords.lat, waypoint.sale_point.coords.lon],
                'sale_point_legal' : waypoint.sale_point.legal_name,
                'sale_point_radius' : waypoint.sale_point.coords.radius,
                'status' : waypoint.status,
                'period' : waypoint.period,
                'local_startdate' : utils.date_to_str(waypoint.local_startdate)
            }
        }

    except InvalidId:
        response = {
            'status' : False,
            'message' : 'Неверный формат id сотрудника.'
        }

    except EmptyRequiredFillException as emptyRequiredFillException:
        response = {
            'status' : False,
            'message' : emptyRequiredFillException.message
        }

    return HttpResponse(json.dumps(response))

