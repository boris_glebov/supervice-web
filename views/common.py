# -*- coding: utf-8 -*-
__author__ = 'warlock'

import forms
import django.contrib.auth as auth

from django.http import  HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render
from django.core.mail import send_mail
from django.contrib.auth.decorators import login_required, permission_required

from core.api import CoreApi
from core.dj import CustomBackend
from pymongo.objectid import ObjectId

##
# Splitter on Get or Post request.
##
def method_splitter(request, get, post, *args, **kwargs):
    if request.method == 'POST' and post is not None:
        return post(request, *args, **kwargs)
    elif request.method == 'GET' and get is not None:
        return get(request, *args, **kwargs)
    else:
        raise Http404()

###
# index
###
def index(request):
    """
    Первая страница. С нее начинается работа с сайтом.
    """
    assert request.method == 'GET'

    if not request.user.is_anonymous() and request.user.is_authenticated():
        return HttpResponseRedirect('/monitoring/')

#    return render_to_response('index.html', context_instance=RequestContext(request))
    return render(request, 'index.html', {'id_body' : 'home'})

###
# login
###

def login_get(request):
    assert request.method == 'GET'

    form = forms.LoginForm()

    return render(request, 'accounts/login.html', {'form' : form})

def login_post(request):
    assert request.method == 'POST'

    if request.POST:
        form = forms.LoginForm(request.POST)
        err_message = 'Неверный email или пароль.'
        if form.is_valid():
            user = CustomBackend().authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])

            if user is not None:
                if user.is_active:
                    auth.login(request, user)

                    return HttpResponseRedirect('/monitoring/')
                else:
                    err_message = 'Ваш профиль не активирован.'

        return render(request, 'accounts/login.html', {'form' : form, 'err_message' : err_message})
    else:
        return login_get(request)

###
# register
###
def register_get(request):
    assert request.method == 'GET'

    form = forms.RegisterForm()

    return render(request, 'accounts/register.html', {'form' : form})

def register_post(request):
    assert request.method == 'POST'

    form = forms.RegisterForm(request.POST)
    is_valid = form.is_valid()

    if not is_valid:
        return render(request, 'accounts/register.html',
                                        {
                                            'form' : form,
                                            'is_valid' : is_valid,
                                        })

    api = CoreApi().database_api

    try:
        full_name = form.cleaned_data['real_name'].split()
        user, ex = api.user_create_one_click(
                                    form.cleaned_data['email'],
                                    form.cleaned_data['password'],
                                    full_name[1],
                                    full_name[0],
                                    form.cleaned_data['company_name'])

        if ex is None:
            try:
                send_mail(
                            'Активация на ресурсе h-qub.ru',
                            'Для активации аккаунта перейдите по ссылке: http://{0}/register/activate/{1}/'.format(request.META['HTTP_HOST'],user.settings.activate),
                            'hello@h-qub.ru',
                            [user.username]
                )
            except Exception as ex:
                return HttpResponseRedirect('/account/login/')

            return HttpResponseRedirect('/register/activate/')
        else:
            return render(request, 'accounts/register.html',
                    {
                    'form' : form,
                    'is_valid' : False,
                    'error_message' : 'На сервере произошла не предвиденная ошибка.'
                    })

    except Exception as ex:
        return render(request, 'accounts/register.html',
                {
                'form' : form,
                'is_valid' : False,
                'error_message' : 'На сервере произошла не предвиденная ошибка.'
                })

def activate(request, key):
    result = False
    if key is not None:
        api = CoreApi().database_api
        result = api.user_activate(key)

    return render(request, 'accounts/activation.html', {'key':key, 'result' : result})

###
# account
###
@login_required
def profile(request):
    return render(request, 'accounts/profile.html')

###
# company
###
@login_required
def company_profile(request):
    db_api = CoreApi().database_api

    company_id = request.user.company.id
    return render(request, 'company/profile.html', {
                                                        'employee' :db_api.employee_list(company_id),
                                                        'device_list' : db_api.device_list(company_id)
                                                    })

###
# Employee
###
@login_required
def employee_list(request):
    db_api = CoreApi().database_api

    return render(request, 'company/modal_table_employee.html', {
        'employee' : db_api.employee_list(request.user.company.id)
    })

###
# monitoring
###
@login_required
def monitoring(request):
    db = CoreApi().database_api
    return render(request, 'monitoring/index.html', {
        'employee_list' : db.employee_list(request.user.company.id)
    })

###
# history
###
@login_required
def history(request, employee_id):
    db = CoreApi().database_api
    return render(request, 'history/index.html', {'employee_list' : db.employee_list(request.user.company.id),
                                                  'selected_employee' : ObjectId(employee_id)
                                                  })

###
# logistic
###
def logistic(request):
    return render(request, 'logistics/index.html', {'page' : 'logistic'})

def routing(request):
    return render(request, 'logistics/index.html', {'page' : 'routing'})

###
# test
###
def test(request, template, **args):
    return render(request,template, args)

def url404(request):
    raise Http404()
