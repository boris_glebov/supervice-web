# -*- coding: utf-8 -*-
__author__ = 'warlock'

from django.core.exceptions import ValidationError
from core.api import CoreApi

def promo_code_validate(value):
    if value != '666666':
        raise ValidationError('Неверный промо код.')

def email_exists_validate(value):
    api = CoreApi().database_api
    if api.user_exists(value):
        raise ValidationError('Пользователь с таким e-mail уже существует.')

def full_name_validate(value):
    """
    Полное имя должно состоять из двух слов разделеных пробелом.
    """
    words = value.split()
    if len(words) != 2:
        raise ValidationError('Введите ваши фамилию и имя. Пример: Иванов Иван.')