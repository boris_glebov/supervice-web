# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib.auth.views import logout
import django

from views import common, rest

import settings
django.get_version()
login_template_name = {'template_name' : 'accounts/login.html'}

urlpatterns = patterns('',

    #index
    (r'^$', common.index),
    (r'^index/$', common.index),

    #login
    (r'^login/$', common.method_splitter, {'get' : common.login_get, 'post' : common.login_post}),
    (r'account/$', common.method_splitter, {'get' : common.login_get, 'post' : common.login_post}),
    (r'account/login/$', common.method_splitter, {'get' : common.login_get, 'post' : common.login_post}),
    (r'^accounts/login/$',common.method_splitter, {'get' : common.login_get, 'post' : common.login_post}),

    #logout
    (r'^account/logout/$', logout, {'template_name' : 'accounts/logout.html'}),
    (r'^logout/$', logout)
    ,
    #profile
    (r'^account/profile', common.profile),

    #monitoring
    (r'analystics/$', common.test, {"template":"analystics/index.html"}),
    (r'cabinet/$', common.test, {"template":"cabinet/index.html"}),
    (r'reports/$', common.test, {"template":"reports/index.html"}),


    #company
    (r'^company/$', common.company_profile),
    (r'^company/profile/$', common.company_profile),
    (r'^company/employee/list/$', common.method_splitter, {'get' : common.employee_list, 'post' : None}),
    (r'^company/employee/list-rest/$', rest.get_employee_list), # получаем список сотрдунико в json
#    (r'^company/employee/get/(\d+)/$', common.method_splitter, {'get' : None, 'post' :rest.get_employee }),
    (r'^company/employee/save/$', common.method_splitter, {'get' : None, 'post' : rest.save_employee}),
    (r'^company/employee/refresh/$', rest.refresh_employee),
    (r'^company/employee/remove/(?P<employee_id>\w+)/$', rest.remove_employee),
    (r'^company/device/save/$', rest.save_device),
    (r'^company/device/refresh/$', rest.refresh_devices),
    (r'^company/device/remove/(?P<device_id>\w+)/$', rest.remove_device),


    #monitoring
    (r'^monitoring/$', common.monitoring),
#   (r'^monitoring/get-track/employee/(?P<employee_id>\w+)/date/(?P<date>\w+)/$', rest.get_track),
    (r'^monitoring/get-track/$', rest.get_track),
    (r'^monitoring/get-last-position', rest.get_last_position),
    (r'^monitoring/get-last-poisiton-by-employee', rest.get_last_position_by_employee),

    #logystics
    (r'^logistic/$', common.logistic),
    (r'^logistic/save-sale-point/$', rest.save_sale_point),
    (r'^logistic/get_sale_points/$', rest.get_sale_points),
    (r'^logistic/routing/$', common.method_splitter, {'get' : common.routing, 'post' : None}),
    #routing
    (r'^logistic/routing/add-sale-point-to-route/$', rest.add_sale_point_to_route),
    (r'^logistic/routing/update-waypoint/$', rest.update_waypoint),
    (r'^logistic/routing/get-current-route/$', rest.get_current_route),

    #hisotry
    (r'^history/(?P<employee_id>\w+)/$', common.history),
    (r'^history/$', common.history, {'employee_id' : None}),


    #register
    (r'^account/register/$', common.method_splitter, {'get' : common.register_get, 'post' : common.register_post }),
    (r'^register/$', common.method_splitter, {'get' : common.register_get, 'post' : common.register_post}),
    (r'^register/activate/$', common.activate, {'key' : None}),
    (r'^register/activate/(\w+)/$', common.activate),


    #temponary

    (r'^tour/$', common.test, {"template":"site/tour.html", 'item' : '#nav-tour'} ),
    (r'^pricing/$', common.test,{"template":"site/price.html", 'item' : '#nav-pricing'} ),
    (r'^why/$', common.test,{"template":"empty.html", 'item' : '#nav-why'} ),
    (r'^about/$', common.test,{"template":"site/about.html", 'item' : '#nav-about'} ),
#    (r'^api/', include('core.urls', 'core')),

    (r'^url404/$', common.url404),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
            }),
    )
