# -*- coding: utf-8 -*-
__author__ = 'warlock'

import database.migrations as migrations
import settings
import datetime
import hashlib
import validators
import uuid

from mongokit import *

db = Connection()

class DjangoDocument(Document):
    __database__ = settings.MONGODB_DB
    use_dot_notation = True
    use_autorefs = True

    @property
    def id(self):
        return self._id

@db.register
class Company(DjangoDocument):
    """
    Сущность. Компания
    """
    __collection__ = 'companies'

    structure = {
        'name' : unicode,
        'address':{
            'country' : unicode,
            'city' : unicode,
            'building' : int,
            'notes' : unicode
        },
        'contacts':{
            'phone' : unicode,
            'email' : unicode,
            'site' : unicode,
            'contact_person' : unicode,
        },

        'devices':[],

        'bank':{
            # Рассчетный счет
            'current_account' : unicode
        },

        'notes' : unicode,

        'settings' : {
            'device_limit' : int,
            'manager_limit' : int,
            'agent_limit' : int,

            'is_available' : bool,
            'is_demo_mode' : bool,
        }
    }

    required_fields = ['name']

    @property
    def users(self):
        """
        Получить всех пользователей компании
        """
        return db.User.find({'_company' : self._id})

@db.register
class User(DjangoDocument):
    """
    Пользователь системы. Он же Супервайзер, либо руководитель.
    """
    __collection__ = 'users'

    migration_handler = migrations.UserMigration

    structure = {
        'username' : unicode,
        'password' :unicode,

        '_company' : ObjectId, #id компании в которой работает пользователь

        'fio' : {
            'first_name' : unicode,
            'last_name' : unicode,
            'third_name' : unicode # Отчество
        },

        'settings' :{
            #Код активации
            'activate' : unicode,

            # Роль пользователя  в системе
            'role' : IS(u'administrator', u'moderator', u'director', u'manager', u'agent'),
        },

        # Главный администратор
        'is_superuser' : bool,
        # Доступ к админке
        'is_staff' : bool,
        # Доступ к системе
        'is_active' : bool,

        'last_login' : datetime.datetime,
        'date_joined' : datetime.datetime
    }

    indexes = [
        {
            'fields' : ['username', 'password'],
            'unique' : True
        }
    ]

    required_fields = ['username','password', 'settings.role','fio.first_name', 'fio.last_name']

    default_values = {
        'is_staff' : False,
        'is_active' : False,
        'is_superuser' : False,
        'settings.role' : u'agent'
        }

    #==== Свойства ====#

    @property
    def company(self):
        """
        Возвращает компанию в которой работает пользователь
        """
        return db.Company.find_one({'_id' : self._company})

    @company.setter
    def company(self, value):
        """
        Добавить пользователя в компанию 'value'
        """
        self._company = value._id

    def _hash_sha(self, raw_string):
        """
        Хэшируем пришедшую строку алгоритмом sha1
        """
        h = hashlib.new('sha1', self.username)
        return h.hexdigest()

    @property
    def id(self):
        return self._id

    #==== Дополнительные методы ====#

    def set_activate_code(self):
        """
        Устанавливаем код для активации аккаунта
        """
        self.settings.activate = u'{0}'.format(self._hash_sha(self.username))

    #=== Реализация Dajango ====#
    # Спецификация https://docs.djangoproject.com/en/dev/topics/auth/

    def is_anonymous(self):
        """
        Always returns False. This is a way of differentiating User and AnonymousUser objects. Generally,
        you should prefer using is_authenticated() to this method.
        """
        return False

    def is_authenticated(self):
        """
        Always returns True. This is a way to tell if the user has been authenticated. This does not imply any permissions,
        and doesn't check if the user is active - it only indicates that the user has provided a valid username and password.
        """
        return True

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        return u'{0} {1}'.format(self.fio.first_name, self.fio.last_name)

    def set_password(self, raw_password):
        """
        Обновляем пароль. На вход нужно подать не хэшированные данные
        Хэширование происходит по алгоритму sha1

        Eng:

        Sets the user's password to the given raw string, taking care of the password hashing. Doesn't save the User object.
        """
        self.password = u'{0}'.format(self._hash_sha(raw_password))

    def check_password(self, raw_password):
        """
        Returns True if the given raw string is the correct password for the user.
        (This takes care of the password hashing in making the comparison.)
        """
        return self.password == self._hash_sha(raw_password)

    def set_unusable_password(self):
        """
        Marks the user as having no password set. This isn't the same as having a blank string for a password.
        check_password() for this user will never return True. Doesn't save the User object.

        You may need this if authentication for your application takes place against an existing external source such as an LDAP directory.
        """
        pass

    def has_usable_password(self):
        """
        Returns False if set_unusable_password() has been called for this user.
        """
        return True

    def get_group_permissions(self, obj=None):
        """
        Returns a set of permission strings that the user has, through his/her groups.

        If obj is passed in, only returns the group permissions for this specific object.
        """
        return self.group

    def get_all_permissions(self, obj=None):
        """
        Returns a set of permission strings that the user has, both through group and user permissions.

        If obj is passed in, only returns the permissions for this specific object.
        """
        return None

    def has_perm(self, obj=None):
        """
        Returns True if the user has the specified permission, where perm is in the format "<app label>.<permission codename>". (see permissions section below). If the user is inactive, this method will always return False.

        If obj is passed in, this method won't check for a permission for the model, but for this specific object.
        """
        return True

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to the user. If from_email is None, Django uses the DEFAULT_FROM_EMAIL.
        """

        pass

    def get_profile(self):
        """
        Returns a site-specific profile for this user.
        Raises django.contrib.auth.models.SiteProfileNotAvailable if the current site doesn't allow profiles, or django.core.exceptions.ObjectDoesNotExist if the user does not have a profile.
        For information on how to define a site-specific user profile, see the section on storing additional user information below.

        Примечание: Мы возвращает обьект пользователя - settings
        """
        return self.settings

@db.register
class Employee(DjangoDocument):
    """
    Эта модель описывает Торгового представителя.
    """
    __collection__ = 'employee'

    migration_handler = migrations.EmployeeMigration

    structure = {

        'fio' : {
            'first_name' : unicode,
            'last_name' : unicode,
            'third_name' : unicode
        },

        'post' : unicode,

        'contact' : {
             'phone' : unicode,
             'email' : unicode
        },

        'settings' : {
            'preset' : str
        },

        'route' : {
            'waypoints' : [ObjectId],
            'settings' : {
                'start_date' : datetime.datetime,
                'period' : int
            }
        },

        '_company' : ObjectId,
        'is_deleted' : bool
    }

    required_fields = ['fio.first_name', 'fio.last_name', '_company']
    validators = {
        'fio.first_name' : validators.is_empty,
        'fio.last_name' : validators.is_empty,
        'fio.third_name' : validators.is_empty,
    }

    default_values = {
        'is_deleted' : False,
        'settings.preset' : 'twirl#carIcon'
    }

    #==== Свойства ====#

    @property
    def company(self):
        """
        Возвращает компанию в которой работает пользователь
        """
        return db.Company.find_one({'_id' : self._company})

    @property
    def full_name(self):
        return u'{0} {1}'.format(self.fio.last_name, self.fio.first_name)

    @company.setter
    def company(self, value):
        """
        Добавить пользователя в компанию 'value'
        """
        self._company = value._id

@db.register
class Device(DjangoDocument):
    """
    GPS Трекер
    """
    __collection__ = "device"

    structure = {
        'imei' : unicode,
        'model': unicode,
        'is_active' : bool,
        'is_deleted' : bool,
        'owned' : ObjectId,
        'company' : ObjectId
    }

    required_fields = ['imei']
    default_values = {
        'is_active' : True,
        'is_deleted' : False
    }

    @property
    def owned_name(self):
        try:
            employee = db.Employee.find_one({'_id' : self.owned})
            return u'{0} {1}'.format(employee.fio.last_name, employee.fio.first_name)
        except Exception:
            return ''

    @property
    def owned_id(self):
        try:
            return self.owned
        except Exception:
            return ''

@db.register
class GpsRecord(DjangoDocument):
    """
    Запись с gps-трэкера.
    """
    __collection__ = 'gps_records'
    migration_handler = migrations.GpsRecordMigration

    structure = {
        'imei' : unicode,

        'lat' : float,
        'lon' : float,
        'raw_lat' : unicode,
        'raw_lon' : unicode,

        'speed' : float,
        'curs' : float,

        'device_mode' : int,
        'report_type' : unicode,
        'alarm_status' : int,
        'geofence_status' : unicode,
        'gps_fix' : int,

        'local_datetime' : datetime.datetime,
        'utc_datetime' : datetime.datetime,
        'rec_datetime' : datetime.datetime,

        'point_type' : str,

        'employee' : ObjectId,
        'device' : ObjectId
    }

    indexes = [
        {
            'fields' : [('employee', INDEX_ASCENDING), ('local_datetime', INDEX_DESCENDING)],
            'unique' : True
        }
    ]

@db.register
class SalePoint(DjangoDocument):
    """
    Торговые точки
    """
    __collection__ = 'sale_point'

    structure = {
        'name' : unicode,
        'legal_name' : unicode,  #Юридическое название компании
        'address' : unicode,
        'region' : unicode,
        'coords':
        {   'lat' : float,
            'lon' : float,
            'radius' : float
        },
        '_user' : ObjectId,
        '_company' : ObjectId
    }

    required_fields = ['name', 'address']

    # Extension:

    @property
    def company(self):
        """
        Возвращает компанию в которой работает пользователь
        """
        return db.Company.find_one({'_id' : self._company})

    @company.setter
    def company(self, value):
        """
        Добавить пользователя в компанию 'value'
        """
        self._company = value._id

    @property
    def user(self):
        """
        """
        return db.User.find_one({'_id' : self._user})

    @user.setter
    def user(self, value):
        self._user = value._id

@db.register
class WayPoint(DjangoDocument):
    """
    Маршруты
    """
    __collection__ = 'way_points'

    structure = {
        'employee_id' : ObjectId,
        'sale_point_id' : ObjectId,
        'status' : bool,
        'period' : int,
        'local_startdate' : datetime.datetime,
        'history' : [{
            'local_datetime' : datetime.datetime,
            'status' : bool,
            'period' : int # Каждые n дней. Где n - значение поля.
        }]
    }

    # Extension:

    @property
    def employee(self):
        return db.Employee.find_one({'_id' : self.employee_id})

    @employee.setter
    def employee(self, value):
        self.employee_id = value

    @property
    def sale_point(self):
        return db.SalePoint.find_one({'_id' : self.sale_point_id})

    @sale_point.setter
    def sale_point(self, value):
        self.sale_point_id = value








