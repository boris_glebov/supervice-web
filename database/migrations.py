# -*- coding: utf-8 -*-
__author__ = 'warlock'

from mongokit import *

class CompanyMigration(DocumentMigration):
    pass

class UserMigration(DocumentMigration):
    pass

class GpsRecordMigration(DocumentMigration):
    def migration01_add_speed_and_curs_field(self):
        self.target = {'speed' : {'$exists' : False}, 'curs' : {'$exists' : False}}
        self.update = {'$set':{'speed' : 0.0, 'curs' : 0.0}}

    def migration02_add_point_type_field(self):
        self.target = {'point_type' : {'$exists' : False}}
        self.update = {'$set' : {'point_type' : 'car'}}

class EmployeeMigration(DocumentMigration):
    def migration01_add_route_field(self):
        self.target = {'route' : {'$exists' : False}}
        self.update = \
            {'$set' :
                     {'route' :
                              {
                                'waypoints' : [],
                                'period' : -1
                              }
                    }
            }
    def migration02__add_settingdotpreset_field(self):
        self.target = {'settings.preset' : {'$exists' : False}}
        self.update = {
            '$set' : {
                'settings' :
                        {
                            'preset' : 'twirl#carIcon'
                        }
            }
        }