import sys
import os
import os.path

#sys.path.insert(0, os.path.dirname(__file__))
sys.path.append('/var/www')
sys.path.append('/var/www/supervise_web')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

from django.core.handlers.wsgi import WSGIHandler
application = WSGIHandler()
