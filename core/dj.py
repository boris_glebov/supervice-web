# -*- coding: utf-8 -*-
__author__ = 'warlock'

from api import CoreApi

class CustomBackend(object):
    def authenticate(self, username, password):
        """
        Авторизация пользователя
        """
        db_api = CoreApi().database_api

        user = db_api.user_get_by_name(username)

        if user is None:
            return None

        user.backend = 'supervise_web.core.dj.CustomBackend'
        return user if user.check_password(password) else None

    def get_user(self, id):
        return CoreApi().database_api.user_get_by_id(id)