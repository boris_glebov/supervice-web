# -*- coding: utf-8 -*-
__author__ = 'warlock'

class EmptyRequiredFillException(Exception):
    def __init__(self, value):
        self.message = value

    def __str__(self):
        return repr(self.message)


if __name__ == '__main__':
    try:
        raise EmptyRequiredFillException("Хубба бубба!")
    except EmptyRequiredFillException as emptyRequiredFillException:
        print emptyRequiredFillException.message
