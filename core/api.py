# -*- coding: utf-8 -*-
__author__ = 'warlock'

from asq.initiators import *
from database.models import *

import calendar
import logging
import core.utils as utils

# Get an instance of a logger
logger = logging.getLogger(__name__)

class _DatabaseApi(object):
    """
    Обертка над ORM, облегчает работу с отдельными сущностями базы данных, таких как
    Пользователя, Компании, Группы и т.д.
    """
    ###
    # User
    ###

    def user_create(self, username, password, first_name, last_name, company):
        """
        Добавляем в систему нового пользователя
        """
        user = db.User()

        #set auth data
        user.username = username
        user.set_password(password)

        #set fio
        user.fio.first_name = first_name
        user.fio.last_name = last_name
        user.is_active = True

        user.company = company

        #Устанавливаем код активации
        user.set_activate_code()

        try:
            user.validate()
            user.save()
            return user, None
        except Exception as ex:
            return user, ex

    def user_create_one_click(self, username, password, first_name, last_name, company_name):
        """
        Создаем пользователя и компанию за один раз
        """
        company, ex = self.company_create(company_name)

        if ex is not None:
            return None, ex

        user, ex = self.user_create(username, password, first_name, last_name, company)

        if ex is not None:
            return None, ex

        return user, None

    def user_exists(self, username):
        """
        Проверяем существует ли пользователь в БД.
        """
        return self.user_get_by_name(username) is not None

    def user_set_company(self, user, company):
        """
        Связываем пользователя с Компанией
        """
        profile = user.get_profile()
        profile.company = company

        try:
            profile.validate()
            profile.save()
        except Exception as ex:
            return profile, ex

        return profile, None

    def user_set_role(self, user, role_name):
        """
        Задаем роль пользователя в системе
        """
        user.role = role_name

        try:
            user.validate()
            user.save()
        except Exception as ex:
            return user, ex

        return user, None

    def user_get(self, **kwargs):
        """
        Возвращает список пользователей отфильтрованных по заданным критериям (полям)
        """
        return db.User.find(**kwargs)

    def user_get_by_name(self, username):
        """
        Возвращает первого из списка по имени
        """
        return db.User.find_one({'username' : username})

    def user_get_by_id(self, id):
        """
        Получить пользователя по id
        """
        return db.User.find_one({'_id' : id})

    def user_activate(self, code):
        """
        Активируем пользователя
        """
        user = db.User.find_one({'settings.activate' : code})

        if user is None:
            return False

        try:
            user.is_active = True
            user.validate()
            user.save()
        except Exception:
            return False

        return True

    ###
    # Company
    ###
    def company_create(self, company_name):
        """
        Создаем компанию. Если при записи в БД возникнет ошибка пишем в лог.
        Метод возращает новый обьект Company и экземпляр ошибки если такова была.
        """
        company = db.Company()

        company.name = company_name

        try:
            company.validate()
            company.save()
            return company, None
        except Exception as ex:
            return company, ex

    def company_exists(self, company_name):
        """
        Проверяем существование компании
        """
        return db.Company.find_one({'name' : company_name}) is not None

    def company_get(self, **kwargs):
        """
        Возвращает список компаний отфильтрованных по заданным криетриям (полям)
        """
        return db.Company.find(kwargs)

    def company_get_by_name(self, company_name):
        """
        Возвращают первый элемент из списка найденых компаний по заданному имени.
        """
        return query(Company.objects.filter(name=company_name)).first()

    def company_get_by_id(self, id):
        """
        Получаем обьект компания по id.
        """
        return db.Company.find_one({'_id' : id})

    ###
    # Employee
    ###

    def employee_create(self, first_name, last_name, third_name, phone, company):
        employee = db.Employee()
        employee.fio.first_name = first_name
        employee.fio.last_name = last_name
        employee.fio.third_name = third_name

        employee.contact.phone = phone
        employee.company = company

        try:
            employee.validate()
            employee.save()
            return employee, None
        except Exception as ex:
            return employee, ex

    def employee_list(self, company_id, skip=0, limit=50):
        return db.Employee.find({'_company' : company_id, 'is_deleted' : False}).skip(skip).limit(limit)

    def employee_remove(self, employee_id):
        employee = self.employee_get(employee_id)

        if employee is None:
            raise Exception('Сотрудник не найден.')

        try:
            employee.is_deleted = True
            employee.validate()
            employee.save()

            # Удаляем связи с девайсами:
            for device in db.Device.find({'owned':employee.id}):
                device.owned = None
                try:
                    device.validate()
                    device.save()
                except Exception:
                    continue
        except Exception:
            raise Exception("Ошибка. Не удалось удлаить сотрудника.")

    def employee_get(self, employee_id):
        employee = db.Employee.find_one({'_id' : ObjectId(employee_id)})
        return employee

    ###
    # Devices
    ###

    def device_list(self, company_id, skip=0, limit=50):
        return db.Device.find({'company' : company_id, 'is_deleted' : False}).skip(skip).limit(limit)

    def device_get(self, device_id):
        device = db.Device.find_one({'_id' : ObjectId(device_id)})
        return device

    def device_remove(self, device_id):
        """
        Удаляем устройство. Под удалением понимается выставление св-ва is_deleted в True.
        """

        device = self.device_get(device_id)

        if device is None:
            raise Exception('Устройство не найдено.')

        try:
            device.is_deleted = True
            device.validate()
            device.save()
        except Exception:
            raise Exception("Не удалось удалить устройство.")

    def device_exists(self, imei, device_id=None):
        """
        Проверяем существование устройства.
        Решить впорос что возвращать если пытаются узнать существования "удаленного" устройства?
        """

        if device_id is not None:
            device = db.Device.find_one({'imei' : imei, '_id' : {'$ne': device_id}})
        else:
            device = db.Device.find_one({'imei' : imei})

        return device is not None

    def device_create(self, imei, model, is_active, company_id, owned_id):
        """
        Регистрируем в бд новое устройство
        """
        if self.device_exists(imei):
            return None, Exception('Устройство с таким imei уже зарегистрировано.')

        device = db.Device()

        device.imei = imei
        device.model = model
        device.is_active = is_active
        device.company = company_id
        if owned_id:
            device.owned = ObjectId(owned_id)

        try:
            device.validate()
            device.save()

            return device, None
        except Exception:
            return device, Exception('Проверьте правильность введенных данных.')

    ###
    # Sale Points
    ###

    def sale_point_create(self, user, name, coords, address, region, legal_name = '', radius= 15.0 ):
        """
        Новая торговая точка
        """
        sale_point = db.SalePoint()

        sale_point.name = name
        sale_point.legal_name = legal_name

        sale_point.coords.lat = coords[0]
        sale_point.coords.lon = coords[1]
        sale_point.coords.radius = radius

        sale_point.address = address
        sale_point.region = region

        sale_point.user = user
        sale_point.company = user.company

        try:
            sale_point.validate()
            sale_point.save()

            return sale_point, None
        except Exception as ex:
            logger.exception(ex)
            return sale_point, Exception("Внутряння ошибка. Торговая точка не может быть добавлена.")

    def sale_point_list(self, company_id):
        """
        Список торговых точек.
        """
        return db.SalePoint.find({'_company' : company_id})

    def sale_point_exists(self, sale_point_id, company_id):
        """
        Проверяет существование торговой точки
        """
        return db.SalePoint.find_one({'_id' : sale_point_id, '_company' : company_id}) is not None

    def sale_point_get(self, sale_point_id, company_id):
        """
        Запрашивает экземпляр торговой точки по id
        """
        return db.SalePoint.find_one({'_id' : sale_point_id, '_company' : company_id})

    ###
    # WayPoints
    ###

    def waypoint_list(self, employee_id, all = False):
        """
        Список
        """
        date_now = datetime.datetime.now()
        return [{
            'id' : str(waypoint.id),
            'employee_id' : str(waypoint.employee_id),
            'sale_point_id' : str(waypoint.sale_point_id),
            'sale_point_name' : waypoint.sale_point.name,
            'sale_point_address' : waypoint.sale_point.address,
            'sale_point_coords' :  [waypoint.sale_point.coords.lat, waypoint.sale_point.coords.lon],
            'sale_point_legal' : waypoint.sale_point.legal_name,
            'sale_point_radius' : waypoint.sale_point.coords.radius,
            'status' : waypoint.status,
            'period' : waypoint.period,
            'local_startdate' : utils.date_to_str(waypoint.local_startdate)
        } for waypoint in db.WayPoint.find({'employee_id' : employee_id})]

    def waypoint_get(self, employee_id, sale_point_id):
        """
        Получить точку маршрута по id сотрудника и id торговой точки.

        Если точка не найдена, вернуть None.
        """
        return query(db.WayPoint.find({'employee_id' : employee_id, 'sale_point_id' : sale_point_id})).first_or_default(None)

    def waypoint_get_by_id(self, waypoint_id):
        """
        Излвекаем запись точки маршрута по id из БД.
        """
        return db.WayPoint.find_one({'_id' : waypoint_id})

    def waypoint_add(self, employee_id, sale_point_id, period, status, local_startdate):
        """
        Добавляем торговую точку в маршрут сотруднику.
        """
        waypoint = self.waypoint_get(employee_id, sale_point_id)

        if waypoint is not None:
            return waypoint, Exception(u"Точка  '{0}' уже добавлена в маршрут.".format(waypoint.sale_point.name))

        try:
            waypoint = db.WayPoint()

            waypoint.employee_id = employee_id
            waypoint.sale_point_id = sale_point_id
            waypoint.status = status
            waypoint.period = period
            waypoint.local_startdate = local_startdate

            waypoint.history.append({
                'local_datetime' : local_startdate,
                'status' : status,
                'period' : period
            })

            waypoint.validate()
            waypoint.save()

            return waypoint, None

        except Exception as ex:
            return waypoint, Exception('Внутренняя ошибка сервера.')

    def waypoint_update(self, waypoint_id, period, status, local_startdate):
        """
        Обновление данных точки маршрута.

        Возвращает обновленный экземпляр waypoint (точки маршрута) и Ошибку если есть (или None).
        """
        waypoint = self.waypoint_get_by_id(waypoint_id)

        if waypoint is None:
            return None, Exception('Ошибка. Точка маршрута не найдена.')

        try:
            # Сохраняем текущие значения в истории:
            waypoint.history.append({
                'local_datetime' : waypoint.local_startdate,
                'status' : waypoint.status,
                'period' : waypoint.period
            })

            waypoint.status = status
            waypoint.period = period
            waypoint.local_startdate = local_startdate

            waypoint.validate()
            waypoint.save()

            return waypoint, None

        except Exception as ex:
            return waypoint, Exception('Внутренняя ошибка сервера')


class _ServiceApi(object):
    def get_track_per_day(self, employee, date):
        """
        Получить трек сотрудника за один день
        """

        track = db.GpsRecord.find({'employee' : employee.id,
                                    'local_datetime' : {'$gte' : date, '$lte' : datetime.datetime(date.year, date.month, date.day, 23, 59, 59)},
                                   })
        return track

    def get_days_where_exists_track(self, employee_id):
        """
        Возвращает коллекцию дат в которые трэк существует
        """
        date_now = datetime.datetime.now()
        date_start = datetime.datetime(date_now.year, date_now.month, 1)
        date_end = datetime.datetime(date_now.year, date_now.month, calendar.monthrange(date_now.year, date_now.month))

        dates = query(db.GpsRecord.find(
                {
                    'employee' : employee_id,
                    'local_datetime' : {'$gte' : date_start, '$lte' : date_end}
                }))\
        .select(lambda x: x.local_datetime).distinct()

        return dates

    def get_last_position(self, employee):
        """
        Возвращает последнюю полученную точку
        """
        records = db.GpsRecord.find({'employee' : employee.id}).sort('local_datetime', -1)

        geopoint = query(records).first_or_default(None)

        return geopoint

    def get_route(self, employee_id, all = False):
        """
        Получить маршрут пользователя
        """
        date_now = datetime.datetime.now()
        return [{
            'id' : str(waypoint.id),
            'employee_id' : str(waypoint.employee_id),
            'sale_point_id' : str(waypoint.sale_point_id),
            'sale_point_name' : waypoint.sale_point.name,
            'sale_point_address' : waypoint.sale_point.address,
            'sale_point_coords' :  [waypoint.sale_point.coords.lat, waypoint.sale_point.coords.lon],
            'sale_point_legal' : waypoint.sale_point.legal_name,
            'sale_point_radius' : waypoint.sale_point.coords.radius,
            'status' : waypoint.status,
            'period' : waypoint.period,
            'local_startdate' : utils.date_to_str(waypoint.local_startdate)
        } for waypoint in db.WayPoint.find({'employee_id' : employee_id,
                                            'local_startdate' : {'$lte' : datetime.datetime(date_now.year, date_now.month, date_now.day)},
                                            'status' : True,
                                            }) if ((date_now - waypoint.local_startdate).days % waypoint.period) == 0]


class CoreApi(object):
    @property
    def database_api(self):
        return _DatabaseApi()

    @property
    def service_api(self):
        return _ServiceApi()


#CoreApi().database_api.user_create_one_click(u'avatar29A@gmail.com', u'123456',u'Boris', u'Glebov', u'Infinity')

if __name__ == '__main__':
    pass