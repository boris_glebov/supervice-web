# -*- coding: utf-8 -*-
"""
Модуль содержит утилиты для работы с гео координтами
"""

__author__ = 'warlock'
import math
from geopy import distance, Point

#def calc_distance(p1, p2):
#    """
#    Расчет расстояния между двумя гео-точками
#
#    Входные параметры ассоциативный массив:
#    {
#        lat : 72.12922,
#        lon : 83.34222
#    }
#    """
#    return round( 111.2 * math.sqrt(math.pow((p1['lat']-p2['lat']),2)+math.pow((p1['lon']-p2['lon'])*math.cos(math.pi*p1['lat']/180),2)))

def calc_distance(p1, p2):
    return distance.distance(Point('{0},{1}'.format(p1['lat'], p1['lon'])) , Point('{0},{1}'.format(p2['lat'], p2['lon'])))

def optimization_track(track):
    """
    Производит оптимизацию трэка. Обьединяет множество точек находящихся в близости друг от друга в одну.
    Скопление множества точек в одном месте частое явление. Возникает как правило из за того что наблюдаем долго
    находится в одном месте.

    Входные параметры: массив обьектов вида

    {
        lat : 72.12922,
        lon : 83.34222,
        local_datetime : "16.12.20011 17:04"
    }

    Возвращает новый массив.
    """

    prev_point = None
    optimal_track = []
    repeat_point_collection = []
    for p in track:
        p['name'] = extract_time(p['local_datetime'])

        if prev_point is None:
            prev_point = {'lat' : p['lat'], 'lon' : p['lon'], 'origin' : p}
            optimal_track.append(p)
            repeat_point_collection.append(p)
            continue

        dist = calc_distance({'lat' : p['lat'], 'lon' : p['lon']}, prev_point).m

        if dist < 50:
            prev_point['origin']['name'] = '{0} - {1}'.format(
                extract_time(prev_point['origin']['local_datetime']),
                extract_time(p['local_datetime'])
            )
            continue
        elif dist >= 2800: #Если расстояние между точками больше 2км, то явно такой точки быть не может (за 1 минуты даже при скорости в 160км/ч
            continue

        prev_point = \
        {
            'lat' : p['lat'],
            'lon' : p['lon'],
            'origin' : p
        }

        optimal_track.append(p)

    return optimal_track


def extract_time(dt):
    return dt.split(' ')[-1]