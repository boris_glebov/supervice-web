# -*- coding: utf-8 -*-
__author__ = 'warlock'

import datetime

def str_to_date(str_date, format="%d.%m.%Y"):
    """
    Конвертирует строковое представление даты в обьект datetime по заданному формату.
    По умолчанию используется следующий формат: %d.%m.%Y
    Пример валидных данных: 26.06.2012

    Если передать данные не совпадающие с форматом будет выдан эксепшн ValueError
    """
    return datetime.datetime.strptime(str_date, format)

def date_to_str(date):
    """
    Строковое представление даты в формате dd.mm.yyyy
    Пример: 26.06.2012
    """
    return "{0}.{1}.{2}".format(str(date.day).zfill(2), str(date.month).zfill(2), date.year)