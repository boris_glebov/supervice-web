# Django settings for supervise_web project.

import os
from ConfigParser import RawConfigParser

PROJECT_DIR= os.path.abspath(os.path.dirname(__file__))


config = RawConfigParser()
config.read('{0}/settings.ini'.format(PROJECT_DIR))

DEBUG = config.get('server', 'DEBUG').lower() == 'true'
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Boris Glebov', 'admin@h-qub.ru'),
)


MANAGERS = ADMINS


DATABASES = {
    'default': {
        'ENGINE': config.get('database', 'DATABASE_ENGINE'), # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': config.get('database', 'DATABASE_NAME'),                      # Or path to database file if using sqlite3.
        'USER': config.get('database', 'DATABASE_USER'),                      # Not used with sqlite3.
        'PASSWORD': config.get('database', 'DATABASE_PASSWORD'),                  # Not used with sqlite3.
        'HOST': config.get('database', 'DATABASE_HOST'),                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': config.get('database', 'DATABASE_PORT'),                      # Set to empty string for default. Not used with sqlite3.
        'OPTIONS': {'autocommit': True,},
    },
}

MONGODB_DB = config.get('database', 'MONGODB_DB')

###
# Email settings
###
EMAIL_HOST = config.get('email', 'EMAIL_HOST')
EMAIL_PORT = int(config.get('email', 'EMAIL_PORT'))
EMAIL_HOST_USER = config.get('email', 'EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = config.get('email', 'EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = config.get('email', 'EMAIL_USE_TLS').lower() == 'true'
EMAIL_BACKEND = config.get('email','EMAIL_BACKEND')

YANDEX_MAPS_API_KEY = config.get('map', 'YANDEX_MAPS_API_KEY')

AUTH_PROFILE_MODULE = 'database.UserSettings'

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Asia/Yekaterinburg'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(PROJECT_DIR, 'static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '7^fvz+x0&xh$0z1&hip=+z2#dg=vaci85x)(fs(^&5hbm9h@ol'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'supervise_web.context_processors.set_static_url_proc',
    )


MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.transaction.TransactionMiddleware',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (

    os.path.join(PROJECT_DIR, 'templates').replace('\\','/'),

    )

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'supervise_web.database',
    'supervise_web',
    # Uncomment the next line to enable the admin:
#    'django.contrib.admin',

)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },

    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


# ============== DOJO SETTINGS ========================


# ===========
