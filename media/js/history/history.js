/**
 * Created by h-qub.ru
 * User: Glebov Boris
 * Date: 18.05.12
 * Time: 15:22
 */

// Ссылка на экземпляр карты:
var map;

// Содержит текущую дату:
var selected_date;

// Содержит список координат трэка.
var _current_history_track, _current_history_point_collection;

// Содержит коллекцию геообьектов (Placemark и Circle)
var _salepoint_geocollection;

// Метка с именем пользователя по которому смотрим историю.
// Метка находится в точке последнего сохраненого местоположения.
var _current_history_mark;

var _current_employee;

// Коллекция контрольных точек:
var _control_points_collection;

// ViewModel (см. knockout.js)
var _historyViewModel;

var _temp_global_object = {
    // id таймера который овтечает за проигрывание трэка:
    play_track_interval_id : undefined,
    // Кол-во точек которые нужно отрисовать на этой итерации проигрывания трэка:
    playtrack_points_count : undefined,
    // Линия (объект Polyline) изображающий пройденный путь на карте
    distance_travaled_path : undefined
};

// Основной метод в котором происходит связывание html элементов с обработчиками событий.
function bind_controls_with_events(){
    //Настариваем календарь:

   //Выставляем текущую дату:
    var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();

    selected_date = day + '.' + month + '.' + year;
    $('#datepicker').val(selected_date);

    $('#datepicker').datepicker({
//        showOn: "button",
//        buttonImage: "/media/img/calendar.gif",
//        buttonImageOnly: true,
        minDate : new Date(2005, 0, 1),

        onSelect: function(dateText, inst) {
            selected_date = dateText;

            get_track();
        }
    });

    $("#datepicker").datepicker( "option",
        $.datepicker.regional['ru'] );

    $(".alert").alert();

    // Добавляем доп. контролы на карту:
    _initCustomControls();

    // Инициализируем MVVM модель
    _createViewModel();

    //Загруажем торговые точки:
    _getSalePoints();

    //Обрабатываем выбор сотруднкиа:
    $('.emp_item').bind("click", function(event){
        event.preventDefault();

        $(_getSelectedEmployee()).removeClass('active');
        $(this).addClass('active');



        get_track();
    });

    //При клике в любой точке карты скидываем метки:
    map.events.add('click', function(event){
        if(_control_points_collection){
            this.geoObjects.remove(_control_points_collection);
        }
    }, map);

}

// Извлекает со страницы csrf токен. Для post ajax запросов.
function get_csrf_token(){
    return $('*[name="csrfmiddlewaretoken"]').val();
}

// Настраиваем размеры элементов на странице
function sizeSetup()
{
    var deltaMapHeight = 187;

    $('#map').width($(window).width() - ($('#employee-collection').width() + 70));
    $('#map').height($(window).height() - 73);

    $('#map').css('display', 'block');

    var heightEmployeeBlock = $(window).height() - 132;
    $('#dash-left').height(heightEmployeeBlock);
    $('#employee-collection').height(heightEmployeeBlock);
}

// Запрашивает с сервера трэк за указанную дату
// и вызывает метод _drawTrack для отрисовки.
function get_track(){
    var selected_employee = _getSelectedEmployee();

    if(!selected_date || !selected_employee){
        return;
    }

    _clearMap();

    var employee_id = $('.active input').val();

    $.ajax({
        type:"POST",
        url:"/monitoring/get-track/",
        data: {'employee_id':employee_id, 'date' : selected_date, 'csrfmiddlewaretoken' : get_csrf_token()},
        success: function(data){
            try
            {
                var response = $.parseJSON(data);

                if(response.track_by_time.length == 0)
                {
                    alert('За указанную дату данных нет.');
                    return;
                }

                _current_employee = response.employee;

                _current_history_point_collection = response.track_by_time;
                _drawTrack(_current_history_point_collection, map, response.employee);
            }catch(Exception){
                console.log(Exception);
            }
        }
    });
}

// Добавляет доп. элеменыт управления на карту.
function _initCustomControls()
{
    var btnPlayTrack = new ymaps.control.Button('Проиграть трэк');
    btnPlayTrack.events.add('select', function(){
        if(!_current_history_track){
            btnPlayTrack.deselect();
            return;
        }

//        $.map(points_collection, function(el, idx){
//            return {lat : el[0], lon : el[1]};
//        })

        var points_collection = _current_history_point_collection;
//        //Рисуем линию трэка поверх которой будет рисоваться поездка:
        _drawTrack(points_collection, map, _current_employee, {
            is_show_mark: false,
            line : { strokeWidth: 5 }
        });

        _temp_global_object.play_track_interval_id = setInterval(function(){

            //Если точек нет, прекращаем проигровать трэк:
            if(!points_collection || points_collection.length == 0){
                btnPlayTrack.deselect();
                return;
            }

            if(_temp_global_object.playtrack_points_count >= points_collection.length){
                btnPlayTrack.deselect();
                return;
            }

            _clearMap();

            if(!_temp_global_object.playtrack_points_count){
                _temp_global_object.playtrack_points_count = 1;
            }else{
                ++_temp_global_object.playtrack_points_count;
            }

            try
            {
                var track = [];
                for(var i=0; i<_temp_global_object.playtrack_points_count; i++){
                    var point = points_collection[i];
                    if(!point)
                        break;

//                    track.push({lat: point[0], lon : point[1]} );
                    track.push(point );
                }

                _drawTrack(track, map, _current_employee, {is_show_mark: true,
                line : {
                strokeColor: '#33CC00',
                    strokeWidth: 5
            }});

            }catch(Exception){
                console.log(Exception);
                btnPlayTrack.deselect();
            }

        }, 2000);
    });

     btnPlayTrack.events.add('deselect', function(){

         // Сбарсывает все счетчики и временные обьекты:
         if(_temp_global_object.play_track_interval_id){
             clearTimeout(_temp_global_object.play_track_interval_id);
            _temp_global_object.play_track_interval_id = undefined;
            _temp_global_object.playtrack_points_count = undefined;

            //Удаляем линиюп пройденного пути (на карте рисуется зеленым):
            if(_temp_global_object.distance_travaled_path){
                map.geoObjects.remove(_temp_global_object.distance_travaled_path);
                _temp_global_object.distance_travaled_path = undefined;
            }
         }

        alert('Поездка окончена.');

        _clearMap();
        _drawTrack(_current_history_point_collection, map, _current_employee);
     });

    map.controls.add(btnPlayTrack, {right:85, top:5});
}

// Создаем ViewModel
function _createViewModel()
{
    _historyViewModel = {
        sale_point_collection : ko.observableArray([])

    };
}

// Получить список торговых точек
function _getSalePoints()
{
    $.ajax({
        type:"POST",
        url:"/logistic/get_sale_points/",
        data: {'csrfmiddlewaretoken' : get_csrf_token()},
        success: function(data){
            var response = $.parseJSON(data);
            if(!response)
                return;

            _historyViewModel.sale_point_collection(response);

            if(!_salepoint_geocollection){
                _salepoint_geocollection = new ymaps.GeoObjectArray();
                map.geoObjects.add(_salepoint_geocollection);
            }else{
                _salepoint_geocollection.removeAll();
            }

            for (var key in response)
            {
                var point = response[key];

                var placemark = _getPlacemark(point, 'twirl#shopIcon');
                _salepoint_geocollection.add(placemark);
                _salepoint_geocollection.add(placemark.circle);
            }
        }});
}

/*
 Связывает обьект viewmodel с текущим контекстом (DataContext)
 */
function _applyBindViewModel()
{
    ko.applyBindings(_historyViewModel);
}

// Отрисовывает трэк.
function _drawTrack (points, map, employee, options){
    // Настройки по умолчанию:
    if(!options){
        options = {
            fly : true,
            duration : 0,
            is_show_mark : true,
            line : {
                strokeColor : undefined,
                strokeWidth: 5
            }
        }
    }

    if(points.length == 0)
        return;

    var translate_points = [];
    for(var key in points){
        translate_points.push([points[key].lat, points[key].lon]);
    }

    //Создаем линию трэка:
    _current_history_track = new ymaps.Polyline(translate_points, {}, {
        strokeWidth: options.line.strokeWidth, // ширина линии
        strokeColor: options.line.strokeColor
    });

    //При двойном клике по линии выводит ввиде обьектов Placemark
    //контрольные точки, чтобы можно было проконтролировать где и восколько был наблюдаемый.
    _current_history_track.events.add('contextmenu',  function(event){
        _drawControlPoints(this.points);
    },
    {
        'map' : map,
        'points' : points
    });

    //Предотвращаем поднятие событие click до карты
    //Иначе контрольные точки не смогут появится.
    _current_history_track.events.add('click', _handleClickByTrack);

    try
    {
        var last_point = points[points.length-1];

        if(options.is_show_mark)
        {
            _current_history_mark = new ymaps.Placemark([last_point.lat, last_point.lon],
                {
                    iconContent : employee.full_name
                },
                {
                    preset: 'twirl#blueStretchyIcon'
                });

            map.geoObjects.add(_current_history_mark);
            map.panTo([last_point.lat, last_point.lon], {fly  : options.fly, duration: options.duration});
        }else{
            _temp_global_object.distance_travaled_path = _current_history_track;
        }

        map.geoObjects.add(_current_history_track);

    }
    catch(Exception)
    {
        console.log(Exception);
    }
}

// Обработка клика на трэке
// При клике на в любом месте трэка выводим балун в котором написана время нахождения в данной точке
function _handleClickByTrack(event){
    event.preventDefault();
    if(map.balloon.isOpen()){
        map.balloon.close();
    }

    var coords = event.originalEvent.target.geometry.getCoordinates();
    var click_coord = event.get('coordPosition');
    var prev_coord = undefined;

    var actual_point = undefined;
    for(var i=0; i<coords.length; i++){
        if(!prev_coord){
            prev_coord = coords[i];
            continue;
        }

        //Проверяем широту:
        // Дано: click_coord коордианты точки на линии куда кликнул пользователь
        // prev_coord начальная точка отрезка
        // coords[i] конечная точка отрезка
        //
        // Логика: Если широта меньше или равна широте начальной точки то она должна быть
        // меньше или равна широте конечной точки. Правило ассоциативно.
        // Проверка долготы аналогична.
        var lat_check = (click_coord[0] <= prev_coord[0] && click_coord[0] >= coords[i][0])
            || (click_coord[0] > prev_coord[0] && click_coord[0] < coords[i][0]);

        //Проверяем долготу:
        var lon_check = (click_coord[1] <= prev_coord[1] && click_coord[1] >= coords[i][0]) ||
            (click_coord[1] > prev_coord[1] && click_coord[1] >= coords[i][0]);

        if(lat_check && lon_check)
        {
            var check = Math.abs(click_coord[0] - prev_coord[0] + click_coord[1] - prev_coord[1]);
            var label =  '<div>Время проезда: ' + _extract_time(_current_history_point_collection[i-1].local_datetime) + ' - '
                + _extract_time(_current_history_point_collection[i].local_datetime) + '</div>'
                + '<div>Скорость ' + _current_history_point_collection[i].speed + ' км/ч</div>';
            if(!actual_point){
                actual_point =
                {
                    'label' : label ,
                    'check' : check
                };
            }
            else if(actual_point.check > check)
            {
                actual_point.check = check;
                actual_point.label =  label;
            }
        }

        prev_coord = coords[i];
    }

    map.balloon.open(click_coord, actual_point.label);
}

// Извлекаем время из local_datetime
// Формат: 16.08.2012 16:24
function _extract_time(dt){
    return dt.split(' ')[1];
}

// Очищаем карту от гео обьектов.
function _clearMap(){
    var objects = [_current_history_mark, _current_history_track, _control_points_collection];
    for(var key in objects)
        _removeFromMapGeoObjects(map, objects[key]);

    _current_history_track = _current_history_mark  = _control_points_collection = undefined;
}

// Безопасное удаление объекта с карты
function _removeFromMapGeoObjects(map, object){
    try
    {
        if(object)
        {
            map.geoObjects.remove(object);
            object = undefined;
        }
    }
    catch(Exception)
    {
        console.log(Exception);
    }
}

// Извлекает со страницы выбранного сотрдуника.
// !В дальнейшем требуется рефакторинг по MVVM шаблону. (using knockoutjs).
function _getSelectedEmployee(){
    var employee_collection = $('.emp_item.active');
    if(employee_collection.length == 0)
        return undefined;
    else
        return employee_collection.get(0);
}

/*
 Добавляет на карту placemark зеленого цвета.
 Метка описывает торговую точку добавленную менеджером.
 */
function _getPlacemark(point, preset){

    if(!preset){
        preset = 'twirl#blueStretchyIcon';
    }

    var placemark = new ymaps.Placemark(point.coords);
    placemark.properties.set('hintContent', point.name);
//    placemark.properties.set('name', point.address);
//    placemark.properties.set('address', point.address);
    placemark.properties.set('id', point.id);
//    placemark.properties.set('company_name', point.name);
//    placemark.properties.set('owner_name', point.legal_name);
//    placemark.properties.set('description', point.region);
    placemark.properties.set('position', point.coords);
    placemark.properties.set('radius', point.radius);

    placemark.options.set('preset', preset);

    placemark.circle = new ymaps.Circle([point.coords, point.radius]);

//    _handleClickEventPlacemark(placemark);

    return placemark;
}

// Рисует контрольные точки по пути следования.
// В этом же методе происходит оптимизация geo-координат.
function _drawControlPoints(points){

    if(_control_points_collection){
        map.geoObjects.remove(_control_points_collection);
    }

    _control_points_collection = new ymaps.GeoObjectCollection();

    var prevDateTime;
    for(var key in points)
    {
        //dt = Math.round(Math.abs((new Date(getDateFromFormat("25.05.2012 03:10", 'dd.MM.yyyy HH:mm'))).getTime() - new Date().getTime()) / (1000 * 60 ))
//        var local_datetime =new Date(getDateFromFormat(points[key].local_datetime, 'dd.MM.yyyy HH:mm'));
//        if(prevDateTime && Math.round(Math.abs(local_datetime.getTime() - prevDateTime.getTime()) / 60000) < 4){
//            continue;
//        }
//        prevDateTime = local_datetime;

        var p = points[key];

        var placemark = new ymaps.Placemark([p.lat, p.lon],
            {
                iconContent : p.name//p.lat + '; ' + p.lon//p.local_datetime
            },
            {
                preset: 'twirl#blueStretchyIcon'
            });

        placemark.events.add('click', function(event){
           event.preventDefault();

           _control_points_collection.remove(this);
        }, placemark);

        _control_points_collection.add(placemark);
    }

    map.geoObjects.add(_control_points_collection);
}