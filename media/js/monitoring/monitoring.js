/**
 * Created by h-qub.ru
 * User: Glebov Boris
 * Date: 23.04.12
 * Time: 9:17
 */

var MAX_ZOOM = 20;
var UPDATE_TIME = 60000;

var employee = [];

var _monitoringViewModel = undefined;

function get_csrf_token(){
    return $('*[name="csrfmiddlewaretoken"]').val();
}

function bind_controls_with_events()
{
    _createViewModel();

    _monitoringViewModel.placemark_collection.options.set(
        {
            balloonMaxWidth: 300,
            balloonMinWidth: 250,
            balloonContentBodyLayout : 'hqub#placemarklayout'
        });

    //Сохраняем шаблон балуна для метки:
    ymaps.layout.storage.add('hqub#placemarklayout', _getBalloonTemplate());

    map.geoObjects.add(_monitoringViewModel.placemark_collection);

    map.events.add("click", function(event){
        if(!_monitoringViewModel.selected_employee())
            return;

        _monitoringViewModel.selected_employee().placemark.balloon.close();
    })
}


// Устанавливаем размер элементов страницы в зависимости от разрешения экрана.
function sizeSetup()
{
    $('#map').width($(window).width() - ($('#employee-collection').width() + 40));
    $('#map').height($(window).height() - 77);

    $('#map').css('display', 'block');

    var heightEmployeeBlock = $(window).height() - 124;
    $('.accordion-custom').height(heightEmployeeBlock);
    $('#tabContent').height(heightEmployeeBlock);
}

var is_first_run = true;

function _createViewModel()
{
    _monitoringViewModel = {
        selected_employee : ko.observable(undefined),
        employee_under_supervision : ko.observable(undefined),
        employee_collection : ko.observableArray([]),
        placemark_collection : new ymaps.GeoObjectArray(),

        // Найти сотрудника:
        find_employee : function()
        {
            this.placemark.balloon.open();

            setCenter([this.lat, this.lon]);
        },

        // Открыть историю:
        open_history: function()
        {
            window.open('/history/' + this.employee.id);
        },

        show_track : function()
        {
           if(_monitoringViewModel.showed_track){
               map.geoObjects.remove(_monitoringViewModel.showed_track);
           }

            var geometry = [];
            for(var i=0; i<this.track.length; i++)
            {
                var point = this.track[i];
                geometry.push([point.lat, point.lon]);
            }

            _monitoringViewModel.showed_track = new ymaps.Polyline(geometry, {}, {
                draggable: false,
                strokeWidth: 4
            });

            setCenter(geometry[geometry.length-1]);

            map.geoObjects.add(_monitoringViewModel.showed_track);
        },

        // Следить за сотрудником:
        spy : function()
        {
            var under_supervision_employee = _monitoringViewModel.employee_under_supervision();
            if(under_supervision_employee && under_supervision_employee.employee.id == this.employee.id)
            {
               stop_employee_monitoring();
                return;
            }else if (under_supervision_employee)
                stop_employee_monitoring();

            start_employee_monitoring(this);
        },

        // Вызывает при выборе нового сотрудника из списка:
        select_employee : function()
        {
            _monitoringViewModel.selected_employee(this);
        }
    };

   ko.bindingHandlers.uniqueId = {
        init: function(element, valueAccessor)
        {
            var value = valueAccessor();
            element.id = 'accordion_content_' + value;
        },
        counter: 0,
        prefix: "content_accordion_"
    };

   ko.bindingHandlers.uniqueHref = {
        init: function(element, valueAccessor)
            {
                var value = valueAccessor();
                element.setAttribute("href", '#accordion_content_' + value);
            }
        };

   _applyBinding();
}

function _applyBinding()
{
    ko.applyBindings(_monitoringViewModel);
}

// Получить сотрудника из коллекции _monitoringViewModel.employee_collection.
// Если сотрудника с необходимом id нет, вернуть undefined.
function get_employee(employee_id)
{
    var items = $(_monitoringViewModel.employee_collection()).filter(function(idx){
        return _monitoringViewModel.employee_collection()[idx].employee.id == employee_id;
    });

    return items.length > 0 ? items.get(0) : undefined;
}

// Получить обновления по перемещению сотрудников:
function update_all_employee()
{
    $.ajax({
        type:"POST",
        url:"/monitoring/get-last-position/",
        data: {'csrfmiddlewaretoken' : get_csrf_token()},
        success: function(data)
        {
            try
            {
                var response = $.parseJSON(data);

                for(var key in response)
                {
                    var point = response[key];

                    var p = get_employee(point.employee.id);

                    var status_msg = point.last_update_days_count >= 1 ? 'Приемник не работает' : 'Приемник работает';

                    if(p == undefined)
                    {
                        //Добавляем метку на карту.
                        point.placemark = _createEmployeeMark(point);
                        point.track = [{'lat' : point.lat, 'lon' : point.lon}];
                        point.address = ko.observable("Неизвестно");

                        _monitoringViewModel.placemark_collection.add(point.placemark);

                        // Подсказки к статусам:
                        point.battery_text = ko.observable(point.battery + ' %');
                        point.status_msg = ko.observable(status_msg);

                        //Делаем поле battery обзореваемым:
                        point.battery = ko.observable(point.battery);
                        point.last_update_days_count = ko.observable(point.last_update_days_count);
                        //Сохраняем сотрудника в общей коллекции:
                        _monitoringViewModel.employee_collection.push(point);

                        continue;
                    }

                    p.speed = point.speed;
                    p.curs = point.curs;
                    p.date = point.date;

                    p.lat = point.lat;
                    p.lon = point.lon;


                    p.status_msg(status_msg);
                    p.battery_text(point.battery + ' %');
                    p.battery(point.battery);

                    p.last_update_days_count(point.last_update_days_count);

                    p.track.push({lat:p.lat, lon:p.lon});

                    // Перемещаем метку по новым координатам:
                    p.placemark.geometry.setCoordinates([point.lat, point.lon]);


                    if(is_first_run){
                        setCenter([p.lat, p.lon]);
                        is_first_run = false;
                    }
                    //Обновляем данные

                    //Получаем адрес по гео-точке:
                  _setAddressByCoords(p, [point.lat, point.lon]);
                }

                _monitoringViewModel.employee_collection.sort(function(left, right){
                   return left.last_update_days_count() == right.last_update_days_count() ? 0 : (left.last_update_days_count() < right.last_update_days_count() ? -1 : 1)
                });

            }
            catch(Exception)
            {
                console.log(Exception);
            }
        }
    });
}

function _setAddressByCoords(point, coords){
    ymaps.geocode(coords).then(function(res){
        var names = [];
        // Переберём все найденные результаты и
        // запишем имена найденный объектов в массив names
        res.geoObjects.each(function (obj) {
            names.push(obj.properties.get('name'));
        });

        point.address(names.reverse().join(', '));
        point.placemark.properties.set('address', point.address());
    });
}

function _createEmployeeMark(point)
{
    return new ymaps.Placemark([point.lat, point.lon], {
            iconContent : point.employee.full_name,
            hintContent : point.employee.full_name,
            name : point.employee.full_name,
            address : point.address,
            coords : 'Широта: ' + point.lat + ', Долгота: ' + point.lon
        },{
        preset: 'twirl#busIcon'
    });
}


//Ставим сотрудника на мониторинг:
function start_employee_monitoring(employee)
{
    _monitoringViewModel.employee_under_supervision(employee);

    employee.monitoring_coords = [employee.lat, employee.lon];
    employee.placemark.options.set('preset', 'twirl#darkgreenStretchyIcon');

    setCenter([employee.lat, employee.lon]);

    employee.interval = setInterval(function()
    {
        var employee_supervision = _monitoringViewModel.employee_under_supervision();
        // Если не было изменений:
        if(employee_supervision.lat == employee_supervision.monitoring_coords[0] && employee_supervision.lon == employee_supervision.monitoring_coords[1])
            return;

        employee_supervision.monitoring_coords = [employee_supervision.lat, employee_supervision.lon];


        // Удаляем отрисованный трэк лайн:

        map.geoObjects.remove(employee_supervision.track);

        var coordinate = [];
        for(var key in employee_supervision.track){
            coordinate.push([employee_supervision.track[key].lat, employee_supervision.track[key].lon]);
        }

        employee_supervision.track = new ymaps.Polyline(coordinate, {},{
            strokeWidth: 5
        });

        map.geoObjects.add(employee_supervision.track);

        // Позиционируем на точке:
        map.panTo([employee_supervision.lat, employee_supervision.lon], {
            flying:true,
            duration:3000
        });

    }, UPDATE_TIME / 6);
}

function stop_employee_monitoring()
{
    var employee = _monitoringViewModel.employee_under_supervision();
    if(!employee)
        return;


    employee.placemark.options.set('preset', 'twirl#busIcon');
    //Удаляем трэки:
    map.geoObjects.remove(employee.track);

    //Останавливаем таймер
    clearInterval(employee.interval);

    _monitoringViewModel.employee_under_supervision(undefined);
}

// Возвращает html-шаблон контета балуна
function _getBalloonTemplate(){
    var balloonLayout = ymaps.templateLayoutFactory.createClass(
        '<h3>$[properties.name]</h3>' +
            '<p><small><em>$[properties.description]</em></small></p>' +
//            '<p><small><em>$[properties.position]</em></small></p>' +
            '<hr style="margin-top: -10px"/>' +
            '<div><strong>Адрес:</strong> <em>$[properties.address]</em></div>' +
            '<div style="margin-top: 5px;"><strong>Коордианты:</strong> <em>$[properties.coords]</em></div>'
    );

    return balloonLayout;
}

function setCenter(coords)
{
    map.setCenter(coords,  MAX_ZOOM, {checkZoomRange:true});
}