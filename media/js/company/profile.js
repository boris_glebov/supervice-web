/**
 * Created by PyCharm.
 * User: warlock
 * Date: 15.04.12
 * Time: 14:56
 * To change this template use File | Settings | File Templates.
 */

var last_selected_row;

function setup(){
    _setupTable();
    _setupAjaxForms();

    //Инициализируем все контролы-предупреждения:
    $(".alert").alert();

    //Связываем события:
    var employee_actions = function(event){
        event.preventDefault();
        toggleEmployeeView(event.data.is_edit);
    };

    var device_actions = function(event){
        event.preventDefault();
        toggleDeviceView(event.data.is_edit);
    };

    /*
    * Настройка работы с сотрдуником
    */
    //Создание
    $("#create_employee_link").bind("click", {is_edit:false}, employee_actions);
    //Изменение
    $("#edit_employee_link").bind("click", {is_edit:true}, employee_actions);
    //Удаление
    $("#remove_employee_link").bind("click", function(event){
       event.preventDefault();
       removeSelectedRow('/company/employee/remove/',refreshEmployeeTable, 'Выберите сотрудника.');
    });
    //Обновление таблицы сотрудников
    $("#refresh_employee_link").bind("click", function(event){
       event.preventDefault();
        refreshEmployeeTable();
    });

    /*
    * Настройка работы с устройством
    */
    $("#create_device_link").bind("click", {is_edit:false}, device_actions);
    $("#edit_device_link").bind("click", {is_edit:true}, device_actions);
    $("#remove_device_link").bind("click", function(event){
       event.preventDefault();
        removeSelectedRow('/company/device/remove/', refreshDeviceTable, 'Выберите устройство.');
    });
    $("#refresh_device_link").bind("click", function(event){
       event.preventDefault();
        refreshDeviceTable();
    });


    $("#select_device_owned").bind("click", function(event){
       event.preventDefault();
        var params = "menubar=no,location=no,resizable=no,scrollbars=no,status=yes,height=400";
        var dialog = window.open("/company/employee/list/", "Сотрудники", params);
        dialog.cb_selected_row = function(empoyee_id, employee_name){
            $('#device_owned').val(employee_name);
            $('#device_owned_id').val(empoyee_id);
        };
    });
}
function _setupTable(){
    $('tr').bind('click', function(){
        var class_name = 'row-selected';

        $(last_selected_row).toggleClass(class_name);

        $(this).toggleClass(class_name);
        last_selected_row = $(this);
    });
}

function _setupAjaxForms(){

    var _saveCommonBeforeSubmit = function(alert_id, message){
        $(alert_id).html('<div class="alert alert-block alert-error">'+
            '<h4 class="alert-heading">' + message +'</h4></div>');
    };

    /* Employee ajax handle form */

    var _saveEmployeeFormBeforeSubmit = function(formData, jqForm, options){
        form = jqForm[0];

        if(!form.employee_first_name.value || !form.employee_last_name.value){
            _saveCommonBeforeSubmit('#employeeStatusMessage', 'Заполните все обязательные поля');
            return false;
        }

        return true;
    };

    var _saveEmployeeFormSuccess= function(responseText, statusText, xhr, $form){
        try{
            var response = $.parseJSON(responseText);
            if(response.status && !$("#employee_id").val())
            {
                _resetForm();
            }

            $('#employeeStatusMessage').html(response.message);
            refreshEmployeeTable();
        }catch(exception)
        {
            console.log(exception);
        }
    };

    var optionsSaveEmployeeForm = {
//        target:        '#createEmployeeAjaxMessage',   // target element(s) to be updated with server response
        beforeSubmit:  _saveEmployeeFormBeforeSubmit,  // pre-submit callback
        success:       _saveEmployeeFormSuccess  // post-submit callback

        // other available options:
        //url:       '/company/employee/create/'         // override for form's 'action' attribute
        //type:      type        // 'get' or 'post', override for form's 'method' attribute
        //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)
        //clearForm: true        // clear all form fields after successful submit
        //resetForm: true        // reset the form after successful submit

        // $.ajax options can be used here too, for example:
        //timeout:   3000
    };

    $('#employeeForm').ajaxForm(optionsSaveEmployeeForm);

    /* Device ajax handle form */

    var _saveDeviceFormBeforeSubmit = function(formData, jqForm, options){
        form = jqForm[0];

        if(!form.device_imei.value || !form.device_model.value){
            _saveCommonBeforeSubmit('#deviceStatusMessage', 'Заполните все обязательные поля');
            return false;
        }

        return true;
    };

    var _saveDeviceFormSuccess = function(responseText, statusText, xhr, $form){
        try{
            var response = $.parseJSON(responseText);
            if(response.status && !$("#device_id").val()){
                _resetForm();
            }

            $('#deviceStatusMessage').html(response.message);
            refreshDeviceTable();
        }catch(exception)
        {
            console.log(exception);
        }
    };

    var optionsSaveDeviceForm={
        beforeSubmit: _saveDeviceFormBeforeSubmit,
        success: _saveDeviceFormSuccess
    };

    $('#deviceForm').ajaxForm(optionsSaveDeviceForm);

}

function toggleEmployeeView(is_edit){
    /*
    * Отображаем или закрываем диалог
    */

    //Убираем предудыщее статус сообщение:
    $('#employeeStatusMessage').html('');

    var update_view = function(){
        if(!last_selected_row){
            alert("Выберите сотрудника из списка.");
            return;
        }

        //Изменяем название кнопки:
        $('#employee_submit').text('Сохранить');

        //Заполняем форму:
        _fillEmployeeView();

        $('#EmployeeView').modal('toggle');
    };

    var insert_view = function(){
        //Изменяем название кнопки:
        $('#employee_submit').text('Добавить');

        //Очищаем данные:
        _resetForm();

        $('#EmployeeView').modal('toggle');
    };

    if(is_edit)
        update_view();
    else
        insert_view();
}

function toggleDeviceView(is_edit){

    //Убираем предудыщее статус сообщение:
    $('#deviceStatusMessage').html('');

    var update_view = function(){
        if(!last_selected_row){
            alert("Выберите устройство из списка.");
            return;
        }

        //Изменяем название кнопки:
        $('#device_submit').text('Сохранить');

        //Заполняем форму:
        _fillDeviceView();

        $('#DeviceView').modal('toggle');
    };

    var insert_view = function(){
        //Изменяем название кнопки:
        $('#device_submit').text('Добавить');

        //Очищаем данные:
        _resetForm();

        $('#DeviceView').modal('toggle');
    };

    if(is_edit)
        update_view();
    else
        insert_view();
}

function removeSelectedRow(url,refreshCallBack, errorMessage){
    /*
    * Удалить выбранного сотрудника
    */

    if(!confirm('Удалить запись?'))
        return;

    if(last_selected_row)
    {
        var row_id = _get_id();

        $.get(url + row_id + '/', function(data){
            refreshCallBack();
        });
    }else{
        alert(errorMessage);
    }
}


function _fillEmployeeView()
{
    /*
    * Заполняет и открывает всплывающий диалог для редактирования выбранного сотрудника
    * Данные берутся из last_selected_row
    */
    if(last_selected_row) {
        var querySelectedRow = 'tr.row-selected';

        var getValue = function(field){
            return $($(querySelectedRow + " td[name='td_employee_" + field + "']")[0]).html();
        };

        var first_name = getValue('first_name');
        var last_name = getValue('last_name');
        var third_name = getValue('third_name');
        var phone = getValue('phone');

        $('#employee_first_name').val(first_name);
        $('#employee_last_name').val(last_name);
        $('#employee_third_name').val(third_name);
        $('#employee_phone').val(phone);
        $('#employee_id').val(_get_id());
    }
}

function _fillDeviceView(){
    /*
    * Заполняем форму device данными
    */

    if(last_selected_row){
        var querySelectedRow = 'tr.row-selected';

        var getValue = function(field){
            return $($(querySelectedRow + " td[name='td_device_" + field + "']")[0]).html();
        };

        var device_imei = getValue('imei');
        var device_model = getValue('model');
        var device_active = getValue('is_active');
        var device_owned_id = $(querySelectedRow + ' td>input[name*="td_device_owned_id"]').val();
        var device_owned_name = $(querySelectedRow + ' td>input[name*="td_device_owned_name"]').val();

        $('#device_imei').val(device_imei);
        $('#device_model').val(device_model);
        $('#device_owned_id').val(device_owned_id);
        $('#device_owned').val(device_owned_name);


        var node_device_active = $('#device_active');
        if($.trim(device_active) == 'Включен'){
            node_device_active.attr('checked', true);
//            node_device_active.val(true);
        }
        else{
            node_device_active.attr('checked', false);
//            node_device_active.val(false);
        }
        $('#device_id').val(_get_id());
    }
}

function refreshEmployeeTable(){
    $.get('/company/employee/refresh/', function(data) {
        $('.table-employee tbody').html(data);
        _setupTable();
    });
}

function refreshDeviceTable(){
    $.get('/company/device/refresh/', function(data) {
        $('.table-device tbody').html(data);
        _setupTable();
    });
}

function _resetForm(){
    $('#employeeForm').resetForm();
    $('#employee_id').val('');

    $('#deviceForm').resetForm();
    $('#device_id').val('');
}

function _get_id(){
    /*Возвращает id выбранного сотрудника*/
    return $('tr.row-selected td input').val();
}



