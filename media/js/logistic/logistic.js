/**
 * Created by h-qub.ru
 * User: Glebov Boris
 * Date: 25.05.12
 * Time: 17:58
 */

//Коллекция найденных обьектов по запросу (обьект представляет собой Placemark)
//В эту коллекцию попадают еще не добавленные точки:
var _new_waypoint_collection;

//Коллекция уже добавленных торговых точек:
var _waypoint_collection, _circle_collection;

// Выбарнный placemark в данный момент. Тот над которым открыт Balloon.
var _selected_mark;

// ViewModel (см. knockout.js)
var _logisticViewModel;

// При клике по карте появляется баллун (красный). По логике может быть только один такой баллун.
var _red_mark;

// Связвание контролов с событиями:
function bind_controls_with_events()
{
    _createViewModel();

    //Инициализируем элементы управления:
    _initializeCustomControls();

    //Сохраняем шаблон балуна для метки:
    ymaps.layout.storage.add('hqub#placemarklayout', _getBalloonTemplate());

    $('form').submit(function(){

        var query = $('#textSearch').val();

        //Инициализируем коллекцию торговых точек:
        _createMarkCollection();

        var search_by_company = _logisticViewModel.search_type() == 'company';
        //Выполняем поиск:
        _searchSalePoint(query, search_by_company);

        return false;
    });

    map.events.add("click", function(args){
        args.preventDefault();

        if(_selected_mark) {
            _selected_mark.balloon.close();
            _selected_mark = undefined;
            return;
        }

        if(_red_mark) {
            _new_waypoint_collection.remove(_red_mark);
            _red_mark = undefined;
        }

        var coords = args.get('coordPosition');

        // Отправим запрос на геокодирование
        ymaps.geocode(coords).then(function (res) {
            try{
                var names = [];
                // Переберём все найденные результаты и
                // запишем имена найденный объектов в массив names
                res.geoObjects.each(function (obj) {
                    names.push(obj.properties.get('name'));
                });

                _red_mark = new ymaps.Placemark(coords, {
                    // В качестве контента иконки выведем
                    // первый найденный объект
                    iconContent : names[0],
                    name: names[0],
                    address : names[0]

                }, {
                    preset: 'twirl#redStretchyIcon'
                });

                _handleClickEventPlacemark(_red_mark);

                //Возможна ситуация когда пользователь сразу выбирает нужный адрес на карте до использования Поиска.
                //Поэтому нужно создать временную коллекцию точек самостоятельно.
                if(!_new_waypoint_collection){
                    _new_waypoint_collection = new ymaps.GeoObjectArray();
                    // Задаем наш шаблон для балунов геобъектов коллекции.
                    _new_waypoint_collection.options.set({
                        balloonContentBodyLayout:'hqub#placemarklayout',
                        contentFooterLayout : 'hqub#placemarkFooterLayout',

                        // Максимальная ширина балуна в пикселах
                        balloonMinWidth: 300
                    });

                    map.geoObjects.add(_new_waypoint_collection);
                }

                _new_waypoint_collection.add(_red_mark);

            }catch(Exception)
            {
                console.log(Exception);
            }
        });
    });
}

function sizeSetup()
{
    $('#map').width($(window).width() - ($('#employee-collection').width() + 340));
    $('#map').height($(window).height() - 160);
    $('#map').css('display', 'block');

    var collectionHeight = $(window).height() - 210;
    $('.accordion-custom').height(collectionHeight);
    $('#collection-board').height(collectionHeight);
}

// Извлекам из страницы csrf токен
function get_csrf_token(){
    return $('*[name="csrfmiddlewaretoken"]').val();
}

//Осуществлят поиск
//В зависимотси от значения (true/false) аргумента search_by_company осуществляет поиск
//либо по адресам, либо по компаниям
function _searchSalePoint(query, search_by_company)
{
    //Если выбран поиск по компаниям:
    if(search_by_company){
        _searchCompany(query);
        return;
    }

    //Иначе ищем по адресам:
    search_address(query,function(res){

            var countMark = res.metaData.geocoder.found;
            if(countMark == 0)
                return;

            for (var i = 0; i<countMark; i++)
            {
                try
                {
                    var placemark  = res.geoObjects.get(i);
                    if(!placemark)
                        continue;

//                        placemark.properties.set('position', placemark.geometry.getCoordinates());
                    placemark.properties.set('iconContent', placemark.properties.get('name'));
                    placemark.properties.set('address', placemark.properties.get('name'));

                    placemark.options.set('preset', 'twirl#blueStretchyIcon');

                    _handleClickEventPlacemark(placemark);

                    _new_waypoint_collection.add(placemark);
                }
                catch(Exception)
                {
                    console.log(Exception);
                }
            }

            map.setCenter(_new_waypoint_collection.get(0).geometry.getCoordinates(), 16, {
                checkZoomRange:true,
                duration : 1000
            });

        });
}

//Добавляем собственные элементы управления на карту:
function _initializeCustomControls()
{
    add_clear_button(_clearMap);
}

// Очистка карты:
function _clearMap()
{
    _new_waypoint_collection.removeAll();
}

//Инициализируем коллекцию где будут хранится все временные торговые точки:
function _createMarkCollection(){

    if(_new_waypoint_collection)
    {
        map.geoObjects.remove(_new_waypoint_collection);
    }

    _new_waypoint_collection = new ymaps.GeoObjectArray();
    // Задаем наш шаблон для балунов геобъектов коллекции.
    _new_waypoint_collection.options.set({
        balloonContentBodyLayout:'hqub#placemarklayout',
        contentFooterLayout : 'hqub#placemarkFooterLayout',

        // Максимальная ширина балуна в пикселах
        balloonMaxWidth: 300
    });

    map.geoObjects.add(_new_waypoint_collection);
}

/*
Создаем модель представления
*/
function _createViewModel()
{
    _logisticViewModel =  {
        company_name : ko.observable(""),
        owner_name :  ko.observable(""),
        radius : ko.observable(50),
        selected_sale_point_id : ko.observable(undefined),
        search_type : ko.observable("address"),
        points : ko.observableArray([]),


        save_sale_point : function()
        {
            save_search_point(_selected_mark, {
                company_name : this.company_name(),
                owner_name: this.owner_name(),
                radius: this.radius(),
                id : this.selected_sale_point_id()

            }, function(data){
                var response = $.parseJSON(data);
                if(!response.status){
                    console.log(response.message);
                    return;
                }

                var point = {
                    name : response.name,
                    address : response.address,
                    legal_name : response.legal_name,
                    coords : response.coords,
                    id : response.sale_point_id,
                    radius : response.radius
                };


                if(response.is_update)
                {
                    var idx = $(_logisticViewModel.points()).map(function(idx, el){
                        if(el.id == response.sale_point_id)
                            return idx;
                    }).get(0);

                    _logisticViewModel.points.remove(_logisticViewModel.points()[idx]);

                    //удалем радиус:
                    _circle_collection.remove(_selected_mark.circle);

                    _waypoint_collection.remove(_selected_mark);
                    _selected_mark = undefined;
                }
                else
                {
                    _new_waypoint_collection.remove(_selected_mark);
                    _selected_mark = undefined;
                }

                //Добавляем элемент в коллекцию и сортируем:
                _logisticViewModel.points.push(point);
                _logisticViewModel.points.sort(function(left, right)
                {
                    return left.name == right.name ? 0 : (left.name < right.name ? -1 : 1)
                });

                var placemark = _getGreenPlacemark(point);
                placemark.circle = new ymaps.Circle([point.coords, point.radius]);

                _waypoint_collection.add(placemark);
                _circle_collection.add(placemark.circle);

                // Закрываем балун:
                this.balloon.close();
            });
        },

        show_sale_point_on_map: function(){

            map.panTo(this.coords, {
                checkZoomRange : true,
                flying : true
            });

            for(var idx=0; idx < _waypoint_collection.getLength(); idx++){
                var item = _waypoint_collection.get(idx);
                if(item.real_point.id != this.id)
                    continue;

                item.balloon.open();
                break;
            }
        },

        clear : function(){
            this.company_name('');
            this.owner_name('');
        }
    };

    ko.bindingHandlers.uniqueId = {
        init: function(element, valueAccessor) {
            var value = valueAccessor();
            element.id = 'accordion_content_' + value;
        },
        counter: 0,
        prefix: "content_accordion_"
    };

    ko.bindingHandlers.uniqueHref = {
        init: function(element, valueAccessor) {
            var value = valueAccessor();
            element.setAttribute("href", '#accordion_content_' + value);
        }
    };

    _loadSalePoint();

    _applyBindViewModel();
}

//Загружаем список торговых точек:
function _loadSalePoint()
{
    $.ajax({
            type:"POST",
            url:"/logistic/get_sale_points/",
            data: {
                'csrfmiddlewaretoken' : get_csrf_token()},
            success: function(data)
            {
                try
                {
                    var response = $.parseJSON(data);
                    if(!response)
                        return;

                    _logisticViewModel.points(response);

                    if(!_waypoint_collection){
                        _waypoint_collection = new ymaps.GeoObjectArray();
                        _circle_collection = new ymaps.GeoObjectArray();

                        _waypoint_collection.options.set({
                            balloonContentBodyLayout:'hqub#placemarklayout',
                            contentFooterLayout : 'hqub#placemarkFooterLayout',

                            // Максимальная ширина балуна в пикселах
                            balloonMaxWidth: 300
                        });


                        map.geoObjects.add(_waypoint_collection);
                        map.geoObjects.add(_circle_collection);
                    }else{
                        _waypoint_collection.removeAll();
                        _circle_collection.removeAll();
                    }

                    for(var key in response){
                        var point = response[key];

                        var placemark = _getGreenPlacemark(point);
                        var circle = new ymaps.Circle([point.coords, placemark.properties.get('radius')]);
                        placemark.circle = circle;
                        placemark.real_point = point;

                        _circle_collection.add(circle);
                        _waypoint_collection.add(placemark);
                    }
                }catch(Exception)
                {
                    console.log(Exception);
                }
            }
        }
    );
}

/*
Связывает обьект viewmodel с текущим контекстом (DataContext)
*/
function _applyBindViewModel()
{
    ko.applyBindings(_logisticViewModel);
}

/*
Назначаем обработчик события клика по метки.
Так же обрабатывается два события balloonopen и balloonclose
*/
function _handleClickEventPlacemark(placemark){
    placemark.events.add("click", function(args){

        if(_selected_mark) {
            _selected_mark.balloon.close();
        }

        _selected_mark = placemark;
    });

    placemark.events.add('balloonopen', function(args){
        _logisticViewModel.company_name(placemark.properties.get('company_name'));
        _logisticViewModel.owner_name(placemark.properties.get('owner_name'));

        _logisticViewModel.selected_sale_point_id(placemark.properties.get('id'));

        _logisticViewModel.radius(placemark.properties.get('radius'));

       _applyBindViewModel();

    });

    placemark.events.add('balloonclose', function(args){
       _logisticViewModel.clear();
    });
}

// Возвращает html-шаблон контета балуна
function _getBalloonTemplate(){
    var balloonLayout = ymaps.templateLayoutFactory.createClass(
        '<h3>$[properties.name]</h3>' +
            '<p><small><em>$[properties.description]</em></small></p>' +
//            '<p><small><em>$[properties.position]</em></small></p>' +
            '<hr style="margin-top: -10px"/>' +
            '<div>' +
            '<label><strong>Компания:</strong></strong></label><input data-bind="value:company_name" type="text" style="width: 240px;">' +
            '<label><strong>Владелец:</strong></label><input data-bind="value:owner_name" type="text" style="width: 240px;">' +
            '<label><strong>Радиус:</strong></label><input data-bind="value:radius" type="text" style="width: 40px;"><span style="margin-left: 5px; vertical-align: text-bottom;"><strong>м.</strong></span>' +
            '<button class="btn btn-primary" style="float: right; margin-top: 30px;" data-bind="click: save_sale_point">Сохранить</button>' +
            '</div>'
    );

    return balloonLayout;
}

/*
Добавляет на карту placemark зеленого цвета.
Метка описывает торговую точку добавленную менеджером.
*/
function _getGreenPlacemark(point){
    var placemark = new ymaps.Placemark(point.coords);
    placemark.properties.set('iconContent', point.name);
    placemark.properties.set('hintContent', point.name);
    placemark.properties.set('name', point.address);
    placemark.properties.set('address', point.address);
    placemark.properties.set('id', point.id);
    placemark.properties.set('company_name', point.name);
    placemark.properties.set('owner_name', point.legal_name);
    placemark.properties.set('description', point.region);
    placemark.properties.set('position', point.coords);
    placemark.properties.set('radius', point.radius);

    placemark.options.set('preset', 'twirl#shopIcon');

    _handleClickEventPlacemark(placemark);

    return placemark;
}

// Поиск по компаниям:
function _searchCompany(query){
    search_company(query, function(placemark_collection){
        var first_point = undefined;
        for(var key in placemark_collection)
        {
            var placemark = placemark_collection[key];
            _handleClickEventPlacemark(placemark);
            _new_waypoint_collection.add(placemark);

            if(!first_point)
            {
                first_point = placemark.properties.get('position');
            }
        }
        //Позиционируем карту:
        map.panTo(first_point, {
            checkZoomRange : true,
            flying : true
        });
    });
}

// Проверяем существование точки с заданными координатами в переданной коллекции:
function _existsPointWithCoords(coords, collection){

   var it = collection.getIterator();

    var item = undefined;
    while(item = it.getNext())
    {
        var item_coords = item.geometry.getCoordinates();
        if(item_coords[0] == coords[0] && item_coords[1] == coords[1])
            return true;
    }

   return false;
}

