/**
 * Created by h-qub.ru
 * User: warlock
 * Date: 15.06.12
 * Time: 21:52
 */

// Поиск по компаниям:
function search_company(query, callback){

    $.ajax({
        url : 'http://psearch-maps.yandex.ru/1.x/?text=' + query + '&results=100&format=json&key=ACH4Q08BAAAAEkI9BAIA_1NKy4GKnj0oAOequIqwsygI3fMAAAAAAAAAAABM5c3YPQW4ZU0M5yimzdYWroyCXQ==',
        type:"GET",
        dataType : 'text',
        success:function(response){
            try
            {
                var result = $.parseJSON(response);

                var placemark_collection = [];
                var features = result.response.GeoObjectCollection.featureMember;

                for(var key in  features)
                {
                    var feature = features[key].GeoObject;

                    //Разбираем адрес:
                    var str_format_coords = feature.Point.pos.split(' ');
                    var short_ref_address = feature.metaDataProperty.PSearchObjectMetaData.Address;
                    var address = {
                        'city' : short_ref_address.locality ? short_ref_address.locality : '',
                        'street' : short_ref_address.thoroughfare ? short_ref_address.thoroughfare : '',
                        'house' : short_ref_address.premiseNumber ? short_ref_address.premiseNumber : ''
                    };

                    var full_address = address.city + ',' + address.street + ' ' + address.house;

                    var coords =  [
                        parseFloat(str_format_coords[1].replace(",", ".")),
                        parseFloat(str_format_coords[0].replace(",", "."))
                    ];

                    //Проверяем координаты точки, чтобы она не перекрыла уже добавленную точку клиентом:
//                    if(_existsPointWithCoords(coords, _new_waypoint_collection))
//                        continue;

                    var placemark = new ymaps.Placemark(coords,
                        {
                            'iconContent' : feature.name,
                            'company_name' : feature.name,
                            'name' : feature.name,
                            'description' : full_address,
                            'address' : full_address,
                            'position' : coords

                        }, {
                            preset: 'twirl#blueStretchyIcon'
                        });

                    placemark_collection.push(placemark);
                }

                if(callback){
                    callback(placemark_collection);
                }

            }catch(Exception)
            {
                console.log(Exception);
            }
        }
    })
}

// Поиск по адресу
function search_address(query, callback)
{
    //Иначе ищем по адресам:
    ymaps.geocode(query,
        {
            results : 10
        }).then(callback);
}