/**
 * Created by h-qub.ru
 * User: warlock
 * Date: 15.06.12
 * Time: 16:37
 */


/*Constants*/
SALEPOINT = 'salepoint';
WAYPOINT = 'waypoint';
SEARCHPOINT = 'searchpoint';

// ViewModel (см. knockout.js)
var _routingViewModel;

// Коллекция точек входящих в маршрут:
var _waypoint_geocollection;
var _waypoint_collection = []; //тут хранятся точки полученные от сервера

//Торговые точки с которыми работает компания:
var _salepoint_geocollection;
var _salepoint_collection = []; //координаты торговых точек компании

// Торговые точки найденые черзе поиск. Скорее всего эти компании не в базе клиента:
var _searchpoint_geocollection;
var _searchpoint_collection = [];

//Текущая выбранная метка:
var _current_selected_mark = undefined;


// Коллекция радиусов:
var _circle_waypoint_collection;

function bind_controls_with_events()
{
    _createViewModel();

    //Инициализируем элементы управления:
    _initializeCustomControls();

    //Добавляем основные коллекции с гео обьектами на карту:
    var common_collection_options = {
        balloonMaxWidth: 300,
        balloonMinWidth: 250
    };

    _salepoint_geocollection = new ymaps.GeoObjectArray();
    _salepoint_geocollection.options.set(common_collection_options);

    _waypoint_geocollection = new ymaps.GeoObjectArray();
    _waypoint_geocollection.options.set(common_collection_options);

    _searchpoint_geocollection = new ymaps.GeoObjectArray();
    _searchpoint_geocollection.options.set(common_collection_options);

    _circle_waypoint_collection = new ymaps.GeoObjectArray();

    //Загружаем шаблоны:
    ymaps.layout.storage.add(SALEPOINT_TEMPLATE_NAME, get_balloon_template(SALEPOINT_TEMPLATE_NAME));
    ymaps.layout.storage.add(SEARCHPOINT_TEMPLATE_NAME, get_balloon_template((SEARCHPOINT_TEMPLATE_NAME)));
    ymaps.layout.storage.add(WAYPOINT_TEMPLATE_NAME, get_balloon_template(WAYPOINT_TEMPLATE_NAME));

    //Связываем шаблоны с коллекциями:
    _salepoint_geocollection.options.set('balloonContentBodyLayout', SALEPOINT_TEMPLATE_NAME);
    _searchpoint_geocollection.options.set('balloonContentBodyLayout', SEARCHPOINT_TEMPLATE_NAME);
    _waypoint_geocollection.options.set('balloonContentBodyLayout', WAYPOINT_TEMPLATE_NAME);


    map.geoObjects
        .add(_waypoint_geocollection)
        .add(_salepoint_geocollection)
        .add(_searchpoint_geocollection);

    //Обрабатываем событие поиска:
    $('#searchForm').submit(function(){

        var query = $('#textSearch').val();

        //Удаляем предыдущие результаты с карты:
        _searchpoint_collection = [];
        _searchpoint_geocollection.removeAll();

        var search_by_company = _routingViewModel.search_type() == 'company';
        //Выполняем поиск:
        _searchSalePoint(query, search_by_company);

        return false;
    });

    //Настраиваем модальное окно:
    _routingViewModel.salePointSettingsModal($('#SalePointSettings').modal({
        show : false
    }));
    _routingViewModel.salePointSettingsModal().on('hidden',function(){
        //Очищаем сообщение об ошибке:
        $('#statusMessage').html('');
    });

    //Обрабатываем события смена текущего клиента:
    $('#selectEmployees').change('onchange', function(){
        var selected_employees = _routingViewModel.chosen_employee();
        if(!selected_employees){
            return;
        }

        // Очищаем карту
        // Загружаем точки маршрута и отображаем на карте
       _loadRoute(selected_employees.id);
    });

    // Заружаем список сотрудников:
    // Выполняется следующая цепочка: _loadEmployeeCollection -> _loadRoute -> _loadSalePoint
    _loadEmployeeCollection();

}


function sizeSetup()
{
    $('#map').width($(window).width() - ($('#employee-collection').width() + 340));
    $('#map').height($(window).height() - 160);
    $('#map').css('display', 'block');

    var collectionHeight = $(window).height() - 255;
    $('.accordion-custom').height(collectionHeight);
    $('#collection-board').height(collectionHeight);
}

//Добавить точку в маршрут
function add_sale_point_to_route()
{
    $.ajax({
        type:"POST",
        url:"/logistic/routing/add-sale-point-to-route/",
        data: { employee_id:_routingViewModel.chosen_employee().id,
                sale_point_id : _routingViewModel.selected_point.id(),
                start_date : _routingViewModel.selected_point.settings.startDate(),
                period : _routingViewModel.selected_point.settings.chosenPeriod().code,
                status : _routingViewModel.selected_point.settings.is_active(),
                csrfmiddlewaretoken : get_csrf_token()},
        success: function(data){
            var response = $.parseJSON(data);

            if(!response.status)
            {
                $('#statusMessage').html('<div class="alert alert-block alert-error">'+
                    '<h4 class="alert-heading">' + response.message +'</h4></div>');

                return;
            }

            _addWaypointToRoute(response.waypoint);

            for(var idx=0; idx<_salepoint_collection.length; idx++){

                var item = _salepoint_collection[idx];
                if(item.id != response.waypoint.sale_point_id)
                    continue;

                _removeSalePointFromMap(item);
            }

            $('#SalePointSettings').modal('hide');
        },
        error : function(data){
            $('#statusMessage').html('<div class="alert alert-block alert-error">'+
                '<h4 class="alert-heading">' + 'Идут профилактические работы.' +'</h4></div>');
        }
    });
}

// Создать новую торговую точку и добавить сразу ее в маршрут:
function create_salepoint_and_add_to_route()
{
    $.ajax({
        type:"POST",
        url:"/logistic/routing/create-salepoint-and-add-to-route/",
        data:
        {
            'company_name' : viewModel.company_name,
            'owner_name' : viewModel.owner_name ,
            'radius' : viewModel.radius,
            'lat' : coords[0],
            'lon' : coords[1],
            'sale_point_id' : viewModel.id,
            'address' :  search_point.properties.get('address'),
            'region' :  search_point.properties.get('description'),


            csrfmiddlewaretoken : get_csrf_token()
        },
        success: function(data){

        }
    });
}

function update_waypoint()
{
    var waypoint = _current_selected_mark;

    $.ajax({
        type:"POST",
        url:"/logistic/routing/update-waypoint/",
        data: {
            waypoint_id : _routingViewModel.selected_point.id(),
            start_date : _routingViewModel.selected_point.settings.startDate(),
            period : _routingViewModel.selected_point.settings.chosenPeriod().code,
            status :_routingViewModel.selected_point.settings.is_active(),
            csrfmiddlewaretoken : get_csrf_token()},
        success: function(data){
            var response = $.parseJSON(data);

            if(!response.status){
                $('#statusMessage').html('<div class="alert alert-block alert-error">'+
                    '<h4 class="alert-heading">' + response.message +'</h4></div>');

                return;
            }

            //Удаляем старую точку маршрута:
            _removeWaypointFromRoute(waypoint);
            // Добавляем новую:
            _addWaypointToRoute(response.waypoint);

            $('#SalePointSettings').modal('hide');
        }
    });
}

function _initializeCustomControls()
{
    add_clear_button(_clearMap);

    map.controls.add(_createFilterPointControl(), {left:100, top:5});
}

// Список доступных фильтров торговых точек на карте.
function _createFilterPointControl()
{
    //Question: http://clubs.ya.ru/mapsapi/replies.xml?item_no=27070
    var btnFilters = new ymaps.control.ListBox({
        data : {
            title : 'Фильтр точек'
        }});

    var fltByAll = new ymaps.control.ListBoxItem({data :{content : 'Все'}});

    // Изначально фильтр 'Все' делаем активным:
    _routingViewModel.selected_filter(fltByAll);

    fltByAll.select();
    fltByAll.events.add("click", function(){
        // Отменяем старый выбор:
        _routingViewModel.selected_filter().deselect();

        _routingViewModel.selected_filter(fltByAll);

        map.geoObjects.add(_salepoint_geocollection);
        map.geoObjects.add(_waypoint_geocollection);

        fltByAll.select();
        //Закрываем список:
        btnFilters.collapse();
        btnFilters.setTitle('Фильтр: ' + fltByAll.data.get('content'));

    });

    var fltByWaypoint = new ymaps.control.ListBoxItem({data :{content : 'Только точки маршрута'}});
    fltByWaypoint.events.add("click", function(){
        // Отменяем старый выбор:
        _routingViewModel.selected_filter().deselect();

        _routingViewModel.selected_filter(fltByWaypoint);

        map.geoObjects.add(_waypoint_geocollection);
        map.geoObjects.remove(_salepoint_geocollection);

        fltByWaypoint.select();
        //Закрываем список:
        btnFilters.collapse();
        btnFilters.setTitle('Фильтр: ' + fltByWaypoint.data.get('content'));
    });

    var fltBySalePoint = new ymaps.control.ListBoxItem({data :{content : 'Только торговые точки'}});
    fltBySalePoint.events.add("click", function(){
        // Отменяем старый выбор:
        _routingViewModel.selected_filter().deselect();

        _routingViewModel.selected_filter(fltBySalePoint);

        map.geoObjects.remove(_waypoint_geocollection);
        map.geoObjects.add(_salepoint_geocollection);

        fltBySalePoint.select();
        //Закрываем список:
        btnFilters.collapse();
        btnFilters.setTitle('Фильтр: ' + fltBySalePoint.data.get('content'));
    });

//    btnFilters.items = [fltByAll, fltByWaypoint, fltBySalePoint];

    btnFilters.add(fltByAll).add(fltByWaypoint).add(fltBySalePoint);

    return btnFilters;
}

// Очищаем карту от точек
function _clearMap()
{
    _searchpoint_collection = [];
    _searchpoint_geocollection.removeAll();
}

/*
 Создаем модель представления
 */
function _createViewModel()
{
    var periodCollection = _createPeriodCollection();

    _routingViewModel = {
        search_type : ko.observable("address"),
        salePointSettingsModal : ko.observable(undefined),
        current_search_point : {
            company_name : ko.observable(''),
            owner_name: ko.observable(''),
            radius : ko.observable(0),
            id : ko.observable(undefined),
            lat : ko.observable(0.0),
            lon : ko.observable(0.0),
            address : ko.observable(),
            settings:{
                period : ko.observable(periodCollection),
                chosenPeriod : ko.observable(periodCollection[0]),
                startDate : ko.observable(_getCurrentDate()),
                is_active : ko.observable(true)
            },
            save_point : function(){
                $('#btnSalePointAdd').unbind('click');
                $('#btnSalePointAdd').bind('click', null);
                _routingViewModel.salePointSettingsModal().modal('show');
            },
            clear : function(){
                this.current_search_point.company_name('');
                this.current_search_point.owner_name('');
                this.current_search_point.radius(0);
                this.current_search_point.id(undefined);
            }
        },
        selected_point : {
            company_name : ko.observable(''),
            owner_name: ko.observable(''),
            radius : ko.observable(0),
            id : ko.observable(undefined),
            type : ko.observable(SALEPOINT),
            settings:{
              period : ko.observable(periodCollection),
              chosenPeriod : ko.observable(periodCollection[0]),
              startDate : ko.observable(_getCurrentDate()),
              is_active : ko.observable(true)
            },
            save_point : function()
            {
                switch(_routingViewModel.selected_point.type())
                {
                    case SALEPOINT:
                        $('#btnSalePointAdd').unbind('click');
                        $('#btnSalePointAdd').bind('click', add_sale_point_to_route);
                        $('#btnSalePointAdd').html('Добавить');

                        break;

                    case WAYPOINT:
                       $('#btnSalePointAdd').unbind('click');
                        $('#btnSalePointAdd').bind('click', update_waypoint);
                        $('#btnSalePointAdd').html('Сохранить');

                        break;
                }

                _routingViewModel.salePointSettingsModal().modal('show');

            },
            clear :function(){
//                alert('clear');
            }
        },
        selected_filter : ko.observable(undefined),
        employee_collection : ko.observableArray(), // Список сотрудников
        chosen_employee : ko.observable(undefined), //Выбранный сотрудник
        chosen_route : ko.observableArray([]),
        search_point : function(){
           map.panTo(this.sale_point_coords,{
               checkZoomRange : true,
               flying : true
           });

            var this_point = this;
            var elems = $(_routingViewModel.chosen_route()).filter(function(idx){
                return _routingViewModel.chosen_route()[idx].id == this_point.id;
            });

            if(elems.length > 0)
                elems.get(0).placemark.balloon.open();
        }
    };

    ko.bindingHandlers.uniqueWaypoinItemId = {
        init: function(element, valueAccessor) {
            var value = valueAccessor();
            element.id = 'item_content_' + value;
        },
        counter: 0
    };

    ko.bindingHandlers.uniqueWaypointItemHref = {
        init: function(element, valueAccessor) {
            var value = valueAccessor();
            element.setAttribute("href", '#item_content_' + value);
        }
    };

    _applyBindViewModel();
}

/*
Коллекция с периодичностью посещения клиентов:
*/
function _createPeriodCollection()
{
    return [
        {
            code : 1,
            name : 'раз в неделю'
        },
        {
            code : 2,
            name : 'два раза в неделю'
        },
        {
            code : 3,
            name : 'три раза в неделю'
        }]
}

function _getCurrentDate()
{
    var date = new Date();
    var month = _normalizeNumberStringFormat(date.getMonth() + 1);
    var day = _normalizeNumberStringFormat(date.getDate());
    var year = date.getFullYear().toString();

    return day + '.' + month + '.' + year;
}

function _normalizeNumberStringFormat(value)
{
    if(!value)
        return '00';

    var str_value = value.toString();

    if(str_value.length == 1)
        return '0' + str_value;

    return str_value;
}

/*
 Связывает обьект viewmodel с текущим контекстом (DataContext)
 */
function _applyBindViewModel()
{
    ko.applyBindings(_routingViewModel);
}

function _existsSalePointInRoute(point){
    return $(_routingViewModel.chosen_route()).filter(function(idx){
        return this.sale_point_id == point.id;
    }).length > 0;
}

// Загружаем список сотрудников
function _loadEmployeeCollection()
{
    $.ajax({
        type:"POST",
        url:"/company/employee/list-rest/",
        data:
        {
            'csrfmiddlewaretoken' : get_csrf_token()
        },
        success: function(data)
        {
            var response = $.parseJSON(data);

            if(!response.status){
                console.log(response.status.message);
                return;
            }

            var employee_collection = response.employees;

            for(var key in employee_collection){
                var employee = employee_collection[key];

                if(!_routingViewModel.chosen_employee()){
                    _routingViewModel.chosen_employee(employee);
                }

                _routingViewModel.employee_collection.push(employee);
            }

            //Загружаем текущий маршрут сотрудника:
            _loadRoute(_routingViewModel.chosen_employee().id);

        },
        error: function(data){
            console.log('Internal server error. ' + data)
        }
    });
}

// Загружаем маршрут:
function _loadRoute(employee_id)
{
    $.ajax({
        type:"POST",
        url:"/logistic/routing/get-current-route/",
        data:
        {
            csrfmiddlewaretoken : get_csrf_token(),
            employee_id : employee_id
        },
        success: function(data)
        {
           try
           {
               var response = $.parseJSON(data);

                if(!response.status){
                    alert(response.message);
                }

                _routingViewModel.chosen_route.removeAll();
               _waypoint_geocollection.removeAll();

                for(var key in response.route)
                {
                    var waypoint = response.route[key];

                    _addWaypointToRoute(waypoint);
                }

               // Загружаем торговые точки:
               _loadSalePoint();
           }
           catch(exception)
           {
               console.log(exception);
           }
        },
        error: function(data)
        {
            console.log('Internal server error. ' + data);
        }
    })
}

function _addWaypointToRoute(waypoint)
{
    var placemark = new ymaps.Placemark(waypoint.sale_point_coords,
        {
            iconContent: waypoint.sale_point_name,
            hintContent : waypoint.sale_point_name,
            company_name : waypoint.sale_point_name,
            name : waypoint.sale_point_name,
            description : waypoint.address,
            owner_name : waypoint.sale_point_legal,
            sale_point_id : waypoint.sale_point_id,
            id : waypoint.id,
            radius : waypoint.sale_point_radius,
            settings : {
                period : waypoint.period,
                status : waypoint.status,
                local_startdate : waypoint.local_startdate
            }
        },
        {
            preset : 'twirl#carIcon'
        });

    waypoint.placemark = placemark;
    _handleWayPoint(waypoint);

    _routingViewModel.chosen_route.push(waypoint);
    _waypoint_geocollection.add(placemark);
}

function _removeWaypointFromRoute(waypoint)
{
    _waypoint_geocollection.remove(waypoint.placemark);
    _routingViewModel.chosen_route.remove(waypoint);
}

//Загружаем торговые точки:
function _loadSalePoint()
{
    $.ajax({
        type:"POST",
        url:"/logistic/get_sale_points/",
        data: {
            'csrfmiddlewaretoken' : get_csrf_token()
        },
        success: function(data)
        {
            //Очищаем коллекцию:
            _salepoint_geocollection.removeAll();
            _salepoint_collection = [];

            var response = $.parseJSON(data);

            for(var key in response)
            {
                var point = response[key];

                if(_existsSalePointInRoute(point))
                    continue;

                _addSalePointToMap(point);
            }
        }
    });
}

// Добавляем торговую точку на карту
function _addSalePointToMap(salepoint)
{
    var salepointPlacemark = new ymaps.Placemark(salepoint.coords,
        {
            iconContent: salepoint.name,
            hintContent : salepoint.name,
            company_name : salepoint.name,
            name : salepoint.name,
            description : salepoint.address,
            owner_name : salepoint.legal_name,
            id : salepoint.id,
            radius : salepoint.radius
        },
        {
            preset : 'twirl#shopIcon'
        });

    //Сохраняем ссылку на реальный обьект описывающий торговую точку:
    salepointPlacemark.real_point = salepoint;
    salepoint.placemark = salepointPlacemark;

    _handleSalePoint(salepoint);

    _salepoint_collection.push(salepoint);
    _salepoint_geocollection.add(salepointPlacemark);
}

// Удаляем торговую точку с карты
function _removeSalePointFromMap(salepoint)
{
    _salepoint_geocollection.remove(salepoint.placemark);

    for(var i=0; i<_salepoint_collection.length;i++ )
    {
        if(_salepoint_collection[i]==salepoint)
            _salepoint_collection.splice(i,1);
    }
}

//Осуществлят поиск
//В зависимотси от значения (true/false) аргумента search_by_company осуществляет поиск
//либо по адресам, либо по компаниям
function _searchSalePoint(query, search_by_company)
{
    //Если выбран поиск по компаниям:
    if(search_by_company){
        _searchCompany(query);
        return;
    }

    //Иначе ищем по адресам:
    search_address(query,function(res){
        var countMark = res.metaData.geocoder.found;

        if(countMark == 0)
            return;

        for(var i = 0; i<countMark; i++)
        {
            var placemark  = res.geoObjects.get(i);
            if(!placemark)
                continue;

            placemark.properties.set('iconContent', placemark.properties.get('name'));
            placemark.properties.set('address', placemark.properties.get('name'));

            placemark.options.set('preset', 'twirl#redStretchyIcon');

            _handleSearchPoint(placemark);

            _searchpoint_geocollection.add(placemark);
        }
    });
}

// Поиск по компаниям:
function _searchCompany(query)
{
    search_company(query, function(placemark_collection){
        var first_point = undefined;
        for(var key in placemark_collection)
        {
            var placemark = placemark_collection[key];
            placemark.options.set('preset', 'twirl#redStretchyIcon');

            _handleSearchPoint(placemark);
            _searchpoint_geocollection.add(placemark);

            if(!first_point)
            {
                first_point = placemark.properties.get('position');
            }
        }
        //Позиционируем карту:
        map.panTo(first_point, {
            checkZoomRange : true,
            flying : true
        });
    });
}

function _handleSearchPoint(search_point)
{
    search_point.events.add("balloonopen", function(){

        _routingViewModel.current_search_point.company_name(search_point.properties.get('company_name'));
        _routingViewModel.current_search_point.owner_name(search_point.properties.get('owner_name'));
        _routingViewModel.current_search_point.id(search_point.properties.get('id'));

        _routingViewModel.current_search_point.radius(search_point.properties.get('radius'));


        _applyBindViewModel();
    });

    search_point.events.add('balloonclose', function(){
        search_point.current_search_point.clear();
    });
}

function _handleSalePoint(sale_point)
{
    sale_point.placemark.events.add("balloonopen", function()
    {
        if(_current_selected_mark)
        {
            _current_selected_mark.placemark.balloon.close();
        }

        _current_selected_mark = sale_point;

        var placemark = sale_point.placemark;

        _routingViewModel.selected_point.company_name(placemark.properties.get('company_name'));
        _routingViewModel.selected_point.owner_name(placemark.properties.get('owner_name'));
        _routingViewModel.selected_point.id(placemark.properties.get('id'));

        _routingViewModel.selected_point.radius(placemark.properties.get('radius'));

        _routingViewModel.selected_point.type(SALEPOINT);

        _applyBindViewModel();
    });

    sale_point.placemark.events.add('balloonclose', function(){
        _routingViewModel.selected_point.clear();
        _current_selected_mark = undefined;
    });
}

function _handleWayPoint(way_point)
{
    way_point.placemark.events.add("balloonopen", function(){

        if(_current_selected_mark)
        {
            _current_selected_mark.placemark.balloon.close();
        }

        _current_selected_mark = way_point;
        var placemark = way_point.placemark;

        _routingViewModel.selected_point.company_name(placemark.properties.get('company_name'));
        _routingViewModel.selected_point.owner_name(placemark.properties.get('owner_name'));
        _routingViewModel.selected_point.id(placemark.properties.get('id'));

        _routingViewModel.selected_point.radius(placemark.properties.get('radius'));
        _routingViewModel.selected_point.type(WAYPOINT);

        var settings = placemark.properties.get('settings');
        _routingViewModel.selected_point.settings.chosenPeriod(_routingViewModel.selected_point.settings.period()[settings.period - 1]);
        _routingViewModel.selected_point.settings.is_active(settings.status);
        _routingViewModel.selected_point.settings.startDate(settings.local_startdate);

        _applyBindViewModel();
    });

    way_point.placemark.events.add("balloonclose", function(){
        if(_current_selected_mark)
        {
           _current_selected_mark = undefined;
        }
    });
}

// Извлекам из страницы csrf токен
function get_csrf_token(){
    return $('*[name="csrfmiddlewaretoken"]').val();
}

