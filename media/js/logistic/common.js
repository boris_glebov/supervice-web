/**
 * Created by h-qub.ru
 * User: Glebov Boris
 * Date: 18.06.12
 * Time: 15:05
 */

// Константы
var WAYPOINT_TEMPLATE_NAME = 'waypoint';
var SALEPOINT_TEMPLATE_NAME = 'salepoint';
var SEARCHPOINT_TEMPLATE_NAME = 'searchpoint';

// Добавить в элементы управления карты кнопку "Очистить"
function add_clear_button(callback_clear)
{
    var btnClearAll = new ymaps.control.Button('Очистить карту');
    btnClearAll.events.add('click', function(event){
        callback_clear();
    });

    btnClearAll.events.add('select', function(event){
        btnClearAll.deselect();
    });

    // При смене типа карты изменяем размещение кнопки 'Очистить карту'.
    map.events.add('typechange', function(data)
    {
        map.controls.remove(btnClearAll);

        var newType = data.get('newType');
        var coords = {right:85, top:5};

        switch(newType)
        {
            case 'yandex#publicMap':
                coords.right = 150;
                break;

            case 'yandex#publicMapHybrid':
                coords.right = 159;
                break;

            case 'yandex#hybrid':
                coords.right = 90;
                break;

            case 'yandex#satellite':
                coords.right = 95;
                break;
        }

        map.controls.add(btnClearAll, coords);
    });

    map.controls.add(btnClearAll, {right:85, top:5});
}

//Фабрика шаблонов
function get_balloon_template(template_name)
{
    var template = undefined;

    if(template_name == WAYPOINT_TEMPLATE_NAME || template_name == SALEPOINT_TEMPLATE_NAME)
    {
        var button_name = template_name == SALEPOINT_TEMPLATE_NAME ? 'Добавить' : 'Изменить';

        template = ymaps.templateLayoutFactory.createClass(
            '<h3>$[properties.name]</h3>' +
                '<p><small><em>$[properties.description]</em></small></p>' +
                '<hr style="margin-top: -10px"/>' +
                '<div>' +

                '<div style="margin-top: 5px;">' +
                '<strong>Компания:</strong>' +
                '<span data-bind="text:selected_point.company_name" type="text" style="margin-left: 3px;">' +
                '</div>' +

                '<div style="margin-top: 5px;">' +
                '<strong>Владелец:</strong>' +
                '<span data-bind="text:selected_point.owner_name" type="text" style="width: 240px; margin-left: 3px; margin-top: 5px;">' +
                '</div>' +

                '<div style="margin-top: 5px;">' +
                '<strong>Радиус:</strong>' +
                '<span data-bind="text:selected_point.radius" type="text" style="width: 40px; margin-left: 3px;"><span style="margin-left: 5px; vertical-align: text-bottom;"><strong>м.</strong></span>' +
                '</div>' +

                '<div style="margin-top: 10px;">'+
                '<button class="btn btn-primary"  style="float:left;" data-bind="click: selected_point.save_point">' + button_name + '</button>' +
                '</div>'+
                '</div>'
        );
    }else
    {
        template = ymaps.templateLayoutFactory.createClass(
            '<h3>$[properties.name]</h3>' +
                '<p><small><em>$[properties.description]</em></small></p>' +
//            '<p><small><em>$[properties.position]</em></small></p>' +
                '<hr style="margin-top: -10px"/>' +
                '<div>' +
                '<label><strong>Компания:</strong></strong></label><input data-bind="value:current_search_point.company_name" type="text" style="width: 240px;">' +
                '<label><strong>Владелец:</strong></label><input data-bind="value:current_search_point.owner_name" type="text" style="width: 240px;">' +
                '<label><strong>Радиус:</strong></label><input data-bind="value:current_search_point.radius" type="text" style="width: 40px;"><span style="margin-left: 5px; vertical-align: text-bottom;"><strong>м.</strong></span>' +
                '<div style="margin-top: 10px;">'+
                '<button class="btn btn-primary"  style="float:left;" data-bind="click: current_search_point.save_point">Добавить</button>' +
//                        '<button  class="btn" style="float: left; margin-left: 3px;  margin-top: 2px;" data-bind="click: current_search_point.open_settings"><i class="icon-time" style="vertical-align: middle;"></i></button>' +
                '</div>'+
                '</div>'
        );
    }


    return template;
}

// Сохраняем в БД найденную компанию
function save_search_point(search_point, viewModel, callbackSuccess)
{
    var coords = search_point.geometry.getCoordinates();

    // Сохраняем торговую точку (или создаем если новая)
    $.ajax({
        type:"POST",
        url:"/logistic/save-sale-point/",
        data: {
            'company_name' : viewModel.company_name,
            'owner_name' : viewModel.owner_name ,
            'radius' : viewModel.radius,
            'lat' : coords[0],
            'lon' : coords[1],
            'sale_point_id' : viewModel.id,
            'address' :  search_point.properties.get('address'),
            'region' :  search_point.properties.get('description'),
            'csrfmiddlewaretoken' : get_csrf_token()},
        context : search_point,
        success: callbackSuccess
    });
}