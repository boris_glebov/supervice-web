# -*- coding: utf-8 -*-
__author__ = 'warlock'

from django import forms
from views.form_validators import *

class LoginForm(forms.Form):
    username = forms.Field(widget=forms.TextInput(attrs={'class':'span4'}))
    password = forms.Field(widget=forms.PasswordInput(attrs={'class' : 'span4'}))
    remember_me = forms.Field(widget=forms.CheckboxInput(), required=False)


class RegisterForm(forms.Form):
    real_name = forms.CharField(
                                    max_length=256,
                                    error_messages={'required' : 'Обязательно для заполнения.'},
                                    validators=[full_name_validate]
    )
    email = forms.EmailField(
                                    max_length=256,
                                    error_messages={
                                                        'required' : 'Обязательно для заполнения.',
                                                        'invalid'  : 'Введите действительный e-mail адрес.'
                                                    },
                                    validators=[email_exists_validate]
    )
    password = forms.CharField(
                                    widget=forms.PasswordInput(),
                                    error_messages={'required' : 'Обязательно для заполнения.'})
    company_name = forms.CharField(
                                    max_length=256,
                                    error_messages={'required' : 'Обязательно для заполнения.'})
    company_phone = forms.Field(
                                    widget=forms.TextInput(),
                                    required=False,
                                    error_messages={'required' : 'Обязательно для заполнения.'})
#    promo_code = forms.CharField(
#                                    max_length=50,
#                                    validators=[promo_code_validate],
#                                    error_messages={'required' : 'Обязательно для заполнения.'})

class EmployeeForm(forms.Form):
    employee_frist_name = forms.CharField(
        max_length=256,
        error_messages={'required' : 'Поле Имя не заполнено.'},

    )

    employee_last_name = forms.CharField(
        max_length=256,
        error_messages={'required' : 'Поле Фамилия не заполнено.'}
    )

    employee_third_name = forms.CharField(
        max_length=256,
        error_messages={'required' : 'Поле Отчество не заполнено.'}
    )

    employee_phone = forms.CharField(
        max_length=50,
        required=False
    )
