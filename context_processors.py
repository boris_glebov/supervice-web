# -*- coding: utf-8 -*-
__author__ = 'warlock'

import settings

def set_static_url_proc(request):
    return {
        'STATIC_ROOT' : settings.STATIC_ROOT
    }